import React from 'react';
import ReactDOM from 'react-dom';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import axios from 'axios';
import moment from 'moment';
import Loader from 'react-loader';
import $ from 'jquery';
import './App.css';
import '../../node_modules/react-tabs/style/react-tabs.css';
import '../../public/css/gsdk.css';
import '../../public/css/custom.css';
import '../../public/css/demo.css';
import '../../public/css/relation.css';
import '../../public/css/style.css';




class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {input: ''};
		this.handleChange = this.handleChange.bind(this);
	}
	handleChange(event) {    
		this.setState({input: event.target.value});
	}
	render() {
		
		var navbarBrandStyle = {
			"marginLeft": '150px'
		};
		var navStyle = {
			"flex-direction": "row !important"
		}
		var spanStyleOne = {
			'textTransform': 'capitalize',
			 'color':'white'
		};
		var spanStyleTwo = {
			'textTransform': 'lowercase',
			 'color':'white'
		};

		
		var username = this.props.username;
		console.log(this.props);
		return (
			<div className="App">
				<div className='flex-container'>
						{/*<div className='flex-item-1'>
							<table>
								<tbody>
									<tr>
										<th>Saved Searches</th>
									</tr>
									<tr>
										<td>
											<h2>Developing..</h2>
										</td>
									</tr>
									</tbody>
								</table>
						</div>*/}
						<div className='flex-item-2'>
								<div>
									<h3>Recent Activity</h3>
								</div>
									<Tabs forceRenderTabPanel={true}>
										<TabList>
											<Tab>Keywords</Tab>
											<Tab>Cases</Tab>
											<Tab>Research notes</Tab>
										</TabList>
								
										<TabPanel>
											<Keywords username = {this.props.username}/>
										</TabPanel>
										<TabPanel>
											<Cases username = {this.props.username}/>
										</TabPanel>
										<TabPanel>
											<Notes username = {this.props.username}/>
										</TabPanel>
									</Tabs>
						</div>
						{/*<div className='flex-item-3'>
							<table>
								<tbody>
									<tr>
										<th>Recommendations</th>
									</tr>
									<tr>
										<td>
											<h2>Developing..</h2>
										</td>
									</tr>
									</tbody>
								</table>
						</div>*/}
				</div>
				
			</div>
		);
	}
}



class Keywords extends React.Component{
	
	constructor(props) {
		super(props);
		this.state = {
			keywordsList: [],
			isLoaded: false
		};
	}

	componentDidMount() {
		var _this = this;
		console.log(this.props);
		this.serverRequest = axios.post('http://52.76.250.165:3000/myaccount', {
			'username': this.props.username
		})
		.then(function (response) {
			console.log(response);
			var keywordsList = response.data.userData.searchedQueries.reverse();
			console.log(keywordsList);
			_this.setState({keywordsList:keywordsList,isLoaded: true});

		})
		.catch(function (error) {
			console.log(error);
		}); 
			
	}

	render() {  
		var _this = this;
		return (
			<Loader loaded={this.state.isLoaded}>
				<div>
					{this.state.keywordsList.map((keyWord)=>{
						return (
							<div className='keyWordDiv'>            
								<div className="keyword">
									<span className="glyphicon glyphicon-search query"> <a href={keyWord.type == 'US_judgement'?'/us/search/'+keyWord.search_query:'/search/'+keyWord.search_query} target="_blank">{keyWord.search_query}</a></span>
									<p className='moment'>{moment(keyWord.timestamp).format('LLL')}</p>
								</div>
							</div>
						)
					})}
				</div>
			</Loader>
			);
		
		
	}
	
	
	
}


class Cases extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			cases: []
		};
	}

	componentDidMount() {
		var _this = this;
		console.log(this.props);
		this.serverRequest = axios.post('http://52.76.250.165:3000/myaccount', {
			'username': this.props.username
		})
		.then(function (response) {
			console.log(response);
			var cases = response.data.userData.savedCases.reverse();
			_this.setState({cases:cases});
		})
		.catch(function (error) {
			console.log(error);
		}); 
			
	}

	
	
	render() {  
		
		return (
			<div>
				{this.state.cases.map((caseData)=>{
					return (
						<div className='caseDiv'>            
								<span><a target="#" href={'/render/relation/scr/'+parseInt(caseData.case_id)}>{caseData.title}</a></span>
								<p className='moment'>{moment(caseData.timestamp).format('LLL')}</p>
						</div>
					)
				})}
			</div>
			
			); 
	}
}

class Notes extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			notes: []
		};
	}

	componentDidMount() {
		var _this = this;
		console.log(this.props);
		this.serverRequest = axios.post('http://52.76.250.165:3000/myaccount', {
			'username': this.props.username
		})
		.then(function (response) {
			console.log(response);
			var notes = response.data.userData.savedNotes.reverse();
			_this.setState({notes:notes});
		})
		.catch(function (error) {
			console.log(error);
		}); 
			
	}

	
	
	render() {  
		
		return (
			<div>
				{this.state.notes.map((note)=>{
					return (
						<div className='caseDiv'>            
								<span><a target="#" href={'/loadNote/'+note.tag} id="note">{note.tag}</a></span>
								<p className='moment'>{moment(note.timestamp).format('LLL')}</p>
						</div>
					)
				})}
			</div>
			
			); 
	}
}

ReactDOM.render(<App username = {username}/>, document.getElementById('app'));
