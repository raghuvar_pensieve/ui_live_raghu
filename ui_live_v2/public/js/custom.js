$().ready(function(){


	$(window).on('scroll', gsdk.checkScrollForTransparentNavbar);


	$("#case-content").mCustomScrollbar({theme:"minimal-dark"});
	$("#researchnotes").mCustomScrollbar({theme:"minimal-dark"});
	$("#synopsisnotes").mCustomScrollbar({theme:"minimal-dark"});




	$('.field').on('click', function(e){

		e.preventDefault();

		$this               =   $( this );
		var caseid          =   $this.data('caseid');
		var htmlData        = "<h4>NEW CONTENT APPROPRIATE AUTHORITY "+ caseid +"</h4><p>It is another case of homicidal action by cyanide poisoning. It was perhaps in this case, the guidelines as to the proof of certain facts in It was observed 4:27 PM\n Electoral roll is also helpful for the candidates to assess their chance of success. For reference to the final electoral roll, it is also required by the candidates to enable them to canvass amongst the voters. Availability of a final electoral roll with the candidate is, thus, a matter of great importance for him. There cannot further be any doubt whatsoever that the right to vote having regard to section 62 of the 1951 Act vis-a-vis Article 326 of the Constitution of India is a valuable right. A person in terms of Section 62.</p><p>The 1951 Act is entitled to exercise his right of franchise or is disabled therefrom if his name does or does not find place in the electoral roll. [104-B-C-D] 1.2. In terms of the Registration of electors Rules, 1982, a claim or an objection can be entertained by at least seven days prior to the date of filing of nominations. Sub-section (3) of Section 23 of the 1950 Act in no uncertain terms provides for statutory injunction upon the authorities to make any revisions in the electoral roll after the last date fixed which indisputably having regard to the law laid down by this Court in a number of decisions would mean 3 p.m. of the date of filing the nominations.</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>";
		

		$("#mCSB_1_container").html(htmlData); //load new content inside .mCSB_container

	});



	$('.editable').each(function(){
		this.contentEditable = true;
	});
	

});

var offset = 8;
var from = 0;
var cases = [];
var synopsisarray = [];
var collapse = 0;
var caseType = localStorage.getItem('caseType')?localStorage.getItem('caseType'):0;
var caseArray = ['supremeCourt','mumbaiHC','delhiHC','taxCases'];
var placeholderArray = ['Search over 30000 SC cases','Search over 25000 Mumbai HC cases','Search over 75000 Delhi HC cases'];
var current_case_title_log = "";
var current_case_id_log;

//function for firing respective court search function
$("#searchCasesButton").click(function(){
	var index = localStorage.getItem('caseType')?localStorage.getItem('caseType'):caseType;
	switch(parseInt(index)){
		case 0: searchCases(); return;
		case 1: searchMumbaiCases(); return;
		case 2: searchDelhiCases(); return;
		case 3: searchTaxCases(); return;
		case 4: searchUSCases(); return;
	}
});

function saveCaseType(index){
	localStorage.setItem('caseType',index);
}


function suggestPhrases(keypressed){
	if (( keypressed >= 65 && keypressed <= 90) || keypressed ==8){
					input_text = $(".searchboxinput").val();
					console.log("input_text :: " + input_text);
					var ajaxbody = {
					   "text" : input_text
					};
					$.ajax({
						type:'POST',
						data:ajaxbody,
						url : "/suggestive_phrases",
						dataType: "json"
					}).done(function(resp){
						var options = resp.phrases.map(function(item){
							var option = document.createElement('option');
							option.value = item;
							return option;
						});
						$("#phrase-list").empty().append(options);
						
					});


				}
}



//variable to store search id of the result
var search_id = "";
var search_term = "" ;

function saveWhichSearchClicked(){
	searchterm = $(".searchboxinput").val();
	// whether title search selected or not
	var bytitle = $(".bytitle").is(':checked') ;
	var byjudgement = $(".byjudgement").is(':checked') ;
	var bymachine = $(".bymachine").is(':checked') ;
	localStorage.setItem('searchType',0);
	/*if(byjudgement){
		localStorage.setItem('searchType',0);
	}
	else if(bytitle){
		localStorage.setItem('searchType',1);	
	}
	else if(bymachine){
		localStorage.setItem('searchType',2);
	}*/
	

	localStorage.setItem("current_searchterm",searchterm);
	localStorage.setItem("bytitle", bytitle);
	localStorage.setItem("byjudgement", byjudgement);
	localStorage.setItem("bymachine", bymachine);
}

function checkAuthentication(){
	swal({
		title: 'Login',
		text: 'Please login to continue.',
		html: '<div>Username: <input id="swal-input1" class="swal2-input">' + 'Password: <input id="swal-input2" class="swal2-input" type="password">'+'<a href="/forgotpassword"> Forgot password? </a></div>',
		focusConfirm: false,
		preConfirm: function () {
		    return new Promise(function (resolve) {
		    	var loginData = ({
	                username: $('#swal-input1').val(),
	                password: $('#swal-input2').val()
	            });
				$.ajax({
					type:'POST',
					url: "/login",
					data:loginData,
					dataType: "json"
				}).done(function(resp){
					console.log(resp);
					resolve();
				}).error(function(err){
					swal({
					  title: 'Login Error!',
					  text: 'Invalid username/password.',
					  timer: 1000,
					  width: '80%',
					  onOpen: function () {
					    swal.showLoading()
					  }
					}).then(
					  function () {
					  	location.reload();
					  },
					  // handling the promise rejection
					  function (dismiss) {
					    if (dismiss === 'timer') {
					      location.reload();
					    }
					  }
					)

				});
		    })
		},
		showCancelButton: true,
		cancelButtonText: 'Signup',
		confirmButtonColor: '#042F4F',
		confirmButtonText: 'Login',
		allowOutsideClick: false,
		width: '80%'
	  }).then(function (result) {
	  		location.reload();
		},function(){
			window.location.href = '/signup';
	});
}

function searchTaxCases(){
	//saveCaseType(3)
	saveWhichSearchClicked();
	if(searchterm != ""){
		window.location.href = '/tax/search/' + searchterm;
	}
}

function searchDelhiCases(){
	saveCaseType(2);
	saveWhichSearchClicked();
	if(searchterm != ""){
		window.location.href = '/delhi/search/' + searchterm;
	}
}

function searchMumbaiCases(){
	
	saveCaseType(1);
	saveWhichSearchClicked();
	if(searchterm != ""){
		window.location.href = '/mumbai/search/' + searchterm;
	}
}

function searchUSCases(){
	
	saveWhichSearchClicked();
	if(searchterm != ""){
		window.location.href = '/us/search/' + searchterm;
	}
}


//method for scr search page
function searchCases(){
	saveCaseType(0);
	saveWhichSearchClicked();
	if(searchterm != ""){
			window.location.href = '/search/' + searchterm;
	}
}


function callTaxSearch(){
	//$("#loader").show();
	$('.spinner').show();
	
	var searchterm = $("#searchterm").text();
	localStorage.setItem("current_searchterm",searchterm);
	var  bytitle = localStorage.getItem("bytitle");
	var  bymachine = localStorage.getItem("bymachine");
	var byjudgement = localStorage.getItem("byjudgement");
	var body = {"search_query":searchterm};


	var api_request_url = null ;
	var server_request_url = null ;

	var body = null ;


	if ("" + bytitle + ""  == "true"){
		
		api_request_url = "esSearchTaxTitle" ;
		server_request_url = "/ByTitleTax" ;
		body = {title :searchterm};
	 }
	 else if ("" + byjudgement + "" == "true"){
		
		api_request_url = "essearch_by_judgement";
		server_request_url = "/ByJudgement"
		body = {judgement : searchterm};
	 }
	else if ("" + bymachine + "" == "true"){
		
		api_request_url = "similarByTaxText";
		server_request_url = "/tax_search" ;
		body = {"search_query":searchterm};
	}
	else{
		
		api_request_url = "essearch_by_judgement";
		server_request_url = "/ByJudgement"
		body = {judgement : searchterm};
	}

	var ajaxbody = {
		url: api_request_url,
		body: JSON.stringify(body)
	}

	$.ajax({
		type:'POST',
		url: server_request_url,
		data:ajaxbody,
		dataType: "json"
	}).done(function(resp){
		
		if(resp.hasOwnProperty('error'))
		{
			//$("#loader").hide();
			$('.spinner').hide();
			$(".resultDiv").show();
			$(".casecontent").show();
			$("#srp").append("<label>No similar results found</label>");
		}
		 else if ((resp.similar_cases).length == 0){
			 
			 //$("#loader").hide();
			 $('.spinner').hide();
			 $(".resultDiv").show();
			 $(".casecontent").show();
			 $("#srp").append("<label>No similar results found</label>") ;
		}
		else
		{
			from = 0;
			cases = resp.similar_cases;
			search_id = resp.search_id;
			getDetailData("casesby_tax_ids", function(resp){
				appendTaxResultsHTML(resp)
			});
		}

		return resp;
	}).error(function(err){
		
		return err;
	})
}

function callDelhiSearch(){
	//$("#loader").show();
	$('.spinner').show();

	
	var searchterm = $("#searchterm").text();
	localStorage.setItem("current_searchterm",searchterm);
	var  bytitle = localStorage.getItem("bytitle");
	var byjudgement = localStorage.getItem("byjudgement");
	var  bymachine = localStorage.getItem("bymachine");
	var body = {"search_query":searchterm};


	var api_request_url = null ;
	var server_request_url = null ;

	var body = null ;

	if ("" + bytitle + ""  == "true"){
		
		api_request_url = "esSearchDelhiTitle" ;
		server_request_url = "/ByTitleDelhi" ;
		body = {title :searchterm};
	 }
	 else if ("" + byjudgement + "" == "true"){
		
		api_request_url = "esSearchDelhiJudgement";
		server_request_url = "/ByJudgementDelhi"
		body = {judgement : searchterm};
	 }
	else if ("" + bymachine + "" == "true"){
		
		api_request_url = "delhi_similarbytext";
		server_request_url = "/case/similarByText" ;
		body = {search_query:searchterm};
	}
	else{
		
		api_request_url = "esSearchDelhiJudgement";
		server_request_url = "/ByJudgementDelhi"
		body = {judgement : searchterm};
	}

	var ajaxbody = {
		url: api_request_url,
		body: JSON.stringify(body)
	}

	$.ajax({
		type:'POST',
		url: server_request_url,
		data:ajaxbody,
		dataType: "json"
	}).done(function(resp){
		
		if(resp.hasOwnProperty('error'))
		{
			//$("#loader").hide();
			$('.spinner').hide();
			$(".resultDiv").show();
			$(".casecontent").show();
			$("#srp").append("<label>No similar results found</label>");
		}
		 else if ((resp.similar_cases).length == 0){
			 
			 //$("#loader").hide();
			 $('.spinner').hide();
			 $(".resultDiv").show();
			 $(".casecontent").show();
			 $("#srp").append("<label>No similar results found</label>") ;
		}
		else
		{
			from = 0;
			cases = resp.similar_cases;
			search_id = resp.search_id;
//            appendDelhiResultsHTML(cases);
			getDetailData("casesby_delhi_ids", function(resp){
				appendDelhiResultsHTML(resp)
			});
		}

		return resp;
	}).error(function(err){
		
		return err;
	})
}

function callMumbaiSearch(){
	//$("#loader").show();
	$('.spinner').show();
	
	var searchterm = $("#searchterm").text();
	localStorage.setItem("current_searchterm",searchterm);
	var  bytitle = localStorage.getItem("bytitle");
	var byjudgement = localStorage.getItem("byjudgement");
	var  bymachine = localStorage.getItem("bymachine");
	
	var body = {"search_query":searchterm};


	var api_request_url = null ;
	var server_request_url = null ;

	var body = null ;


	if ("" + bytitle + ""  == "true"){
		
		api_request_url = "esSearchMumbaiTitle" ;
		server_request_url = "/ByTitleMumbai" ;
		body = {title :searchterm};
	 }
	 else if ("" + byjudgement + "" == "true"){
		
		api_request_url = "esSearchMumbaiJudgement";
		server_request_url = "/ByJudgementMumbai"
		body = {judgement : searchterm};
	 }
	else if ("" + bymachine + "" == "true"){
		
		api_request_url = "mum_similarbytext";
		server_request_url = "/case/similarByText" ;
		body = {'search_query':searchterm};
	}
	else{
		
		api_request_url = "esSearchMumbaiJudgement";
		server_request_url = "/ByJudgementMumbai"
		body = {judgement : searchterm};
	}

	var ajaxbody = {
		url: api_request_url,
		body: JSON.stringify(body)
	}

	$.ajax({
		type:'POST',
		url: server_request_url,
		data:ajaxbody,
		dataType: "json"
	}).done(function(resp){
		
		if(resp.hasOwnProperty('error'))
		{
			//$("#loader").hide();
			$('.spinner').hide();
			$(".resultDiv").show();
			$(".casecontent").show();
			$("#srp").append("<label>No similar results found</label>");
		}
		 else if ((resp.similar_cases).length == 0){
			 
			 //$("#loader").hide();
			 $('.spinner').hide();
			 $(".resultDiv").show();
			 $(".casecontent").show();
			 $("#srp").append("<label>No similar results found</label>") ;
		}
		else
		{
			from = 0;
			cases = resp.similar_cases;
			search_id = resp.search_id;
			getDetailData("casesby_mumbai_ids", function(resp){
				appendMumbaiResultsHTML(resp)
			});
		}

		return resp;
	}).error(function(err){
		
		return err;
	})
}

function callUSSearch(){
	//$("#loader").show();

	$('.spinner').show();
	
	var searchterm = $("#searchterm").text();
	localStorage.setItem("current_searchterm",searchterm);
	var  bytitle = localStorage.getItem("bytitle");
	var byjudgement = localStorage.getItem("byjudgement");
	var bymachine = localStorage.getItem("bymachine");
	var body = {"search_query":searchterm};


	var api_request_url = null ;
	var server_request_url = null ;

	var body = null ;


	if ("" + bytitle + ""  == "true"){
		
		api_request_url = "esSearchUSTitle" ;
		server_request_url = "/ByTitleUS" ;
		body = {title :searchterm};
	}
	else if ("" + byjudgement + "" == "true"){
		
		api_request_url = "esSearchUSJudgement";
		server_request_url = "/ByJudgementUS"
		body = {judgement : searchterm};
	}
	else if ("" + bymachine + "" == "true"){
		
		api_request_url = "us_similarbytext";
		server_request_url = "/case/similarByText" ;
		body = {'search_query':searchterm};
	}

	var ajaxbody = {
		url: api_request_url,
		body: JSON.stringify(body)
	}

	$.ajax({
		type:'POST',
		url: server_request_url,
		data:ajaxbody,
		dataType: "json"
	}).done(function(resp){
		console.log(resp);
		if(api_request_url == 'us_similarbytext'){
			if(resp.similar_cases.length==0)
			{
				//$("#loader").hide();
				$('.spinner').hide();
				$(".resultDiv").show();
				$(".casecontent").show();
				$("#srp").append("<label>No similar results found</label>");
			}
			else
			{
				cases = resp.similar_cases;
				search_id = resp.search_id;
				appendUSResultsHTML(cases);	
			}
		}
		else{
			if(resp.case_data.length==0)
			{
				//$("#loader").hide();
				$('.spinner').hide();
				$(".resultDiv").show();
				$(".casecontent").show();
				$("#srp").append("<label>No similar results found</label>");
			}
			else
			{
				cases = resp.case_data;
				search_id = resp.search_id;
				appendUSResultsHTML(cases);	
			}	
		}
		

		return resp;
	}).error(function(err){
		
		return err;
	})
}



function callSearch(){
	//$("#loader").show();
	$('.spinner').show();
	var searchterm = $("#searchterm").text();
	localStorage.setItem("current_searchterm",searchterm);
	var  bytitle = localStorage.getItem("bytitle");
	var byjudgement = localStorage.getItem("byjudgement");
	var  bymachine = localStorage.getItem("bymachine");
	var api_request_url = null ;
	var server_request_url = null ;
	

	var body = null ;

	if ("" + bytitle + ""  == "true"){
		
		api_request_url = "esSearch" ;
		server_request_url = "/ByTitle" ;
		body = {title :searchterm};
	 }
	else if ("" + byjudgement + "" == "true"){
		
		api_request_url = "essearch_by_judgement";
		server_request_url = "/ByJudgement"
		body = {judgement : searchterm};
	 }
	else if ("" + bymachine + "" == "true"){
		
		api_request_url = "similarbytext" ;
		server_request_url = "/case/similarByText" ;
		body = {search_query:searchterm};
	}
	else{
		
		api_request_url = "essearch_by_judgement";
		server_request_url = "/ByJudgement"
		body = {judgement : searchterm};
	}
	

	var ajaxbody = {
		url: api_request_url,
		body: JSON.stringify(body)
	}
	
	$.ajax({
		type:'POST',
		url: server_request_url,
		data:ajaxbody,
		dataType: "json"
	}).done(function(resp){
		console.log(resp);
		if(resp.case_data.length==0)
		{
			//$("#loader").hide();
			$('.spinner').hide();
			$(".resultDiv").show();
			$(".casecontent").show();
			$("#srp").append("<label>No similar results found</label>");
		}
		else
		{
			cases = resp.case_data;
			search_id = resp.search_id;
			appendResultsHTML_new(cases);	
		}
		return resp;
	}).error(function(err){
		
		return err;
	})
}

function paginate(){
	
	if(localStorage.getItem("byjudgement")){
		appendResultsHTML_new(cases);
	}
	else{
		getDetailData("casesbyids", function(resp){
			appendResultsHTML(resp)
		});	
	}
	
}

function paginate_us(){
	appendUSResultsHTML(cases);
}

function getDetailData(url, callback){
	var req_url = url;
	var presentcases = [];
	for(i = from; i < parseInt(from+offset) && i < cases.length; i++){
		if(cases[i].case_id != undefined && cases[i].case_id != null){
			presentcases.push(cases[i].case_id)
		}
		else{
			presentcases.push(parseInt(cases[i]))
		}
	}
	var body = {"cases":presentcases};
	var ajaxbody = {
		url: req_url,
		body: JSON.stringify(body)
	}
	$.ajax({
		type:'POST',
		url: "/ajax",
		data:ajaxbody,
		dataType: "json"
	}).done(function(resp){
		from += offset;
		callback(resp);
		return resp;
	}).error(function(err){
		console.log(err);
		return err;
	})
}


function appendTaxResultsHTML(resp){
	
	// $("#srp").html("");
	var current_searchterm = localStorage.getItem("current_searchterm");
	$("#searchCasestext").val(current_searchterm);

	var temp = "";
	$.each(resp, function(key, value){
		var card = '<div class="card card-horizontal"><div class="row"><div class="col-md-10 col-sm-10" style="border-right:1px solid #eee;"><div class="content" style="padding-left:10px"><a href="/relation/tax/'+value.corpus_id+'"><h4 class="title" >'+value.title+'</h4><strong class="title"><b>Subject - </b>'+value.tax_law_type.substring(0,300)+'...</strong><br/><br/><p class="description"><b>Head Note - </b>'+value.hint.substring(0,600)+'...</p><p class="category">' + ' '+value.date+'</p></div></div> </a><div class="col-md-2 col-sm-2" style="padding-left:0px;"><div class="searcharrow col-md-12 col-sm-12" onclick="showdetailsArrowClicked(\''+ "casesby_tax_ids" +'\'   , \''+value.corpus_id+'\')"><i class="fa fa-chevron-right" aria-hidden="true" style="font-size:2.1em;color:#678660;"></i></div><input class="feedbackcaseid" type="hidden" value="'+value.corpus_id+'" /><div class="col-md-12 col-sm-12 individual_feedback_container"><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,1)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,1,'+value.corpus_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,2)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,2,'+value.corpus_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,3)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,3,'+value.corpus_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,4)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,4,'+value.corpus_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,5)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,5,'+value.corpus_id+')" aria-hidden="true"></span></div></div></div></div>'
		temp += card;
	});
	
	showdetailsArrowClicked("casesby_tax_ids", resp[0].corpus_id);

	$("#srp").append(temp);
	//$("#loader").hide();
	$('.spinner').hide();
	$(".resultDiv").show();
	$(".casecontent").show();
	$("#loadmorebutton").show();
	$("#feedbackbutton").show();
}

function appendDelhiResultsHTML(resp){
	
	// $("#srp").html("");
	var current_searchterm = localStorage.getItem("current_searchterm");
	$("#searchCasestext").val(current_searchterm);

	var temp = "";
	$.each(resp, function(key, value){
		var card = '<div class="card card-horizontal"><div class="row"><div class="col-md-10 col-sm-10" style="border-right:1px solid #eee;"><div class="content" style="padding-left:10px"><a href="/relation/delhi/'+value.case_id+'"><h4 class="title" style="font-weight:bold;">'+value.case_title+'</h4><strong class="title"><b>Case Name - </b>'+value.case_name.substring(0,300)+'...</strong><br/><br/><p class="description"><b>Judgement - </b>'+value.judgement.substring(0,630)+'...</p><p class="category">'+value.judge+' '+value.date_of_judgement+'</p></div></div> </a><div class="col-md-2 col-sm-2" style="padding-left:0px;"><div class="searcharrow col-md-12 col-sm-12" onclick="showdetailsArrowClicked(\''+ "casesby_delhi_ids" +'\' , \''+value.case_id+'\')"><i class="arrow fa fa-chevron-right " aria-hidden="true" style="font-size:2.1em"></i></div><input class="feedbackcaseid" type="hidden" value="'+value.case_id+'" /><div class="col-md-12 col-sm-12 individual_feedback_container"><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,1)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,1,'+value.case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,2)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,2,'+value.case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,3)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,3,'+value.case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,4)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,4,'+value.case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,5)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,5,'+value.case_id+')" aria-hidden="true"></span></div></div></div></div>'
		temp += card;
	});
	
	showdetailsArrowClicked("casesby_delhi_ids", resp[0].case_id);

	$("#srp").append(temp);
	//$("#loader").hide();
	$('.spinner').hide();
	$(".resultDiv").show();
	$(".casecontent").show();
	$("#loadmorebutton").show();
	$("#feedbackbutton").show();
}

function appendMumbaiResultsHTML(resp){

	
	// $("#srp").html("");
	var current_searchterm = localStorage.getItem("current_searchterm");
	$("#searchCasestext").val(current_searchterm);

	var temp = "";
	$.each(resp, function(key, value){
		var card = '<div class="card card-horizontal"><div class="row"><div class="col-md-10 col-sm-10" style="border-right:1px solid #eee;"><div class="content" style="padding-left:10px"><a href="/relation/mumbai/'+value.case_id+'"><h4 class="title" style="font-weight:bold;">'+value.case_title+'</h4><strong class="title"><b>Case Name - </b>'+value.case_name.substring(0,300)+'...</strong><br/><br/><p class="description"><b>Judgement - </b>'+value.judgement.substring(0,300)+'...</p><p class="category">'+value.judge+' '+value.date_of_judgement+'</p></div></div> </a><div class="col-md-2 col-sm-2" style="padding-left:0px;"><div class="searcharrow col-md-12 col-sm-12" onclick="showdetailsArrowClicked(\''+ "casesby_mumbai_ids" +'\' , \''+value.case_id+'\')"><i class="arrow fa fa-chevron-right " aria-hidden="true" style="font-size:2.1em"></i></div><input class="feedbackcaseid" type="hidden" value="'+value.case_id+'" /><div class="col-md-12 col-sm-12 individual_feedback_container"><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,1)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,1,'+value.case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,2)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,2,'+value.case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,3)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,3,'+value.case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,4)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,4,'+value.case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,5)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,5,'+value.case_id+')" aria-hidden="true"></span></div></div></div></div>'
		temp += card;
	});
	
	showdetailsArrowClicked("casesby_mumbai_ids", resp[0].case_id);

	$("#srp").append(temp);
	//$("#loader").hide();
	$('.spinner').hide();
	$(".resultDiv").show();
	$(".casecontent").show();
	$("#loadmorebutton").show();
	$("#feedbackbutton").show();
}

function appendUSResultsHTML(resp){
	
	var current_searchterm = localStorage.getItem("current_searchterm");
	$("#searchCasestext").val(current_searchterm);
	console.log(resp);
	var searchType = localStorage.getItem("bymachine");
	var temp = "";
	
	if("" + searchType + "" == "true"){
		if(resp.length<offset){
			offset = resp.length;
		}

		for(var i=from;i<from+offset;i++)		
			
			{	
				var caseData = resp[i];
				if(caseData.html_with_citations.length!=0){
					console.log('cit: '+ i);
					var usJudgement = caseData.html_with_citations;	
				}
				else if(caseData.html.length!=0){
					console.log('html: '+ i);
					var usJudgement = caseData.html;
				}
				else if(caseData.html_lawbox!=0){
					console.log('law: '+ i);
					var usJudgement = caseData.html_lawbox;
				}
				else{
					var usJudgement = caseData.plain_text;
				}
				/*console.log($(usJudgement).find('p').length);
				var len = $(usJudgement).find('p').length;
				var shortJudgement = $(usJudgement).find('p').toArray();
				if(shortJudgement.length){
					shortJudgement = shortJudgement.reduce(function (a, b) { return a.innerHTML.length > b.innerHTML.length ? a : b; });	
				}
				else{
					shortJudgement = $(usJudgement).prop('innerHTML');
				}
				console.log(shortJudgement);*/
				var shortJudgement = usJudgement.replace(/<[^>]+>/g, '');
				var len = shortJudgement.length;
				var card = '<div class="card card-horizontal"><div class="row"><div class="col-md-10 col-sm-10" style="border-right:1px solid #eee;"><div class="content" style="padding-left:10px"><a href="/relation/us/'+resp[i].case_id_source+'"><h4 class="title" style="font-weight:bold;">'+resp[i].case_title+'</h4><strong class="title"><b>Judgement - </b>'+shortJudgement.substring(0,600)+'...</strong><br/><br/></p> <p class="category" style="overflow-wrap:break-word">'+resp[i].opinions_cited+'</p></div></div> </a><div class="col-md-2 col-sm-2" style="padding-left:0px;"><div class="searcharrow col-md-12 col-sm-12" onclick="showdetailsArrowClicked_new(\'' + "cases_by_us_ids" + '\',  \''+i+'\')"><i class="arrow fa fa-chevron-right " aria-hidden="true" style="font-size:2.1em"></i></div><input class="feedbackcaseid" type="hidden" value="'+resp[i].case_id_source+'" /><div class="col-md-12 col-sm-12 individual_feedback_container"><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,1)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,1,'+resp[i].case_id_source+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,2)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,2,'+resp[i].case_id_source+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,3)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,3,'+resp[i].case_id_source+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,4)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,4,'+resp[i].case_id_source+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,5)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,5,'+resp[i].case_id_source+')" aria-hidden="true"></span></div></div></div></div>';
				temp += card;
			}

		current_case_title_log = resp[from].case_title;
		current_case_id_log = resp[from].case_id_source;
		showRightsideDetails("cases_by_us_ids", resp[from]);
	}
	else{
		if(resp.length<offset){
			offset = resp.length;
		}
		console.log(resp);
		for(var i=from;i<from+offset;i++){		
			
			{	
				var caseData = resp[i]._source;
				if(caseData.html_with_citations.length!=0){
					var usJudgement = resp[i]._source.html_with_citations;	
				}
				else if(caseData.html.length!=0){
					var usJudgement = resp[i]._source.html;
				}
				else if(caseData.html_lawbox!=0){
					var usJudgement = resp[i]._source.html_lawbox;
				}
				else{
					var usJudgement = resp[i]._source.plain_text;
				}
				
				/*var len = $(usJudgement).find('p').length;
				if(len){
					var shortJudgement = $(usJudgement).find('p').toArray();
					shortJudgement = shortJudgement.reduce(function (a, b) { return a.innerHTML.length > b.innerHTML.length ? a : b; });	
				}
				else{
					console.log($(usJudgement).find('pre').toArray());
					var shortJudgement = $(usJudgement).find('pre').toArray();
					shortJudgement = shortJudgement.reduce(function (a, b) { return a.innerHTML.length > b.innerHTML.length ? a : b; });	
				}*/
				var shortJudgement = usJudgement.replace(/<[^>]+>/g, '');
				var len = shortJudgement.length;
				//console.log(shortJudgement);
				var card = '<div class="card card-horizontal"><div class="row"><div class="col-md-10 col-sm-10" style="border-right:1px solid #eee;"><div class="content" style="padding-left:10px"><a href="/relation/us/'+resp[i]._source.case_id_source+'"><h4 class="title" style="font-weight:bold;">'+resp[i]._source.case_title+'</h4><strong class="title"><b>Judgement - </b>'+shortJudgement.substring(50,600)+'...</strong><br/><br/></p> <p class="category" style="overflow-wrap:break-word">'+resp[i]._source.opinions_cited+'</p></div></div> </a><div class="col-md-2 col-sm-2" style="padding-left:0px;"><div class="searcharrow col-md-12 col-sm-12" onclick="showdetailsArrowClicked_new(\'' + "cases_by_us_ids" + '\',  \''+i+'\')"><i class="arrow fa fa-chevron-right " aria-hidden="true" style="font-size:2.1em"></i></div><input class="feedbackcaseid" type="hidden" value="'+resp[i]._source.case_id+'" /><div class="col-md-12 col-sm-12 individual_feedback_container"><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,1)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,1,'+resp[i]._source.case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,2)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,2,'+resp[i]._source.case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,3)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,3,'+resp[i]._source.case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,4)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,4,'+resp[i]._source.case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,5)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,5,'+resp[i]._source.case_id+')" aria-hidden="true"></span></div></div></div></div>';
				temp += card;
			}
			
			
		}
		current_case_title_log = resp[from]._source.case_title;
		current_case_id_log = resp[from]._source.case_id;
		showRightsideDetails("cases_by_us_ids", resp[from]._source);
		//showdetailsArrowClicked("casesbyids", resp[from]._source.case_id);
	}
	
	
	from += offset;   //important to be placed here!
	$("#srp").append(temp);
	//$("#loader").hide();
	$('.spinner').hide();
	$(".resultDiv").show();
	$(".casecontent").show();
	$("#loadmorebutton").show();
	$("#feedbackbutton").show();
}

function appendResultsHTML(resp){
	
	// $("#srp").html("");
	var current_searchterm = localStorage.getItem("current_searchterm");
	$("#searchCasestext").val(current_searchterm);

	var temp = "";
	$.each(resp, function(key, value){
		var card = '<div class="card card-horizontal"><div class="row"><div class="col-md-10 col-sm-10" style="border-right:1px solid #eee;"><div class="content" style="padding-left:10px"><a href="/relation/scr/'+value.case_id+'"><h4 class="title" style="font-weight:bold;">'+value.case_title+'</h4><strong class="title"><b>Subject - </b>'+value.subject.substring(0,300)+'...</strong><br/><br/><p class="description"><b>Head Note - </b>'+value.headnote.substring(0,300)+'...</p><p class="category">'+value.judge+' '+value.date_of_judgement+'</p> <p class="category">'+value.citation+'</p></div></div> </a><div class="col-md-2 col-sm-2" style="padding-left:0px;"><div class="searcharrow col-md-12 col-sm-12" onclick="showdetailsArrowClicked(\'' + "casesbyids" + '\',  \''+value.case_id+'\')"><i class="arrow fa fa-chevron-right " aria-hidden="true" style="font-size:2.1em"></i></div><input class="feedbackcaseid" type="hidden" value="'+value.case_id+'" /><div class="col-md-12 col-sm-12 individual_feedback_container"><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,1)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,1,'+value.case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,2)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,2,'+value.case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,3)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,3,'+value.case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,4)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,4,'+value.case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,5)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,5,'+value.case_id+')" aria-hidden="true"></span></div></div></div></div>'
		temp += card;
	})
	showdetailsArrowClicked("casesbyids", resp[0].case_id);
	$("#srp").append(temp);
	//$("#loader").hide();
	$('.spinner').hide();
	$(".resultDiv").show();
	$(".casecontent").show();
	$("#loadmorebutton").show();
	$("#feedbackbutton").show();
}

function appendResultsHTML_new(resp){
	
	// $("#srp").html("");
	var current_searchterm = localStorage.getItem("current_searchterm");
	$("#searchCasestext").val(current_searchterm);
	
	var searchType = localStorage.getItem("bymachine");
	var temp = "";
	if(resp.length<offset){
		offset = resp.length;
	}
	if("" + searchType + "" == "true"){
		
		for(var i=from;i<from+offset;i++){		
			if(resp[i].subject.length > 20 && resp[i].headnote.length > 20 ){
				
				var card = '<div class="card card-horizontal"><div class="row"><div class="col-md-10 col-sm-10" style="border-right:1px solid #eee;"><div class="content" style="padding-left:10px"><a href="/relation/scr/'+resp[i].case_id+'"><h4 class="title" style="font-weight:bold;">'+resp[i].case_title+'</h4><strong class="title"><b>Subject - </b>'+resp[i].subject.substring(0,300)+'...</strong><br/><br/><p class="description"><b>Head Note - </b>'+resp[i].headnote.substring(0,300)+'...</p><p class="category">'+resp[i].judge+' '+resp[i].date_of_judgement+'</p> <p class="category">'+resp[i].citation+'</p></div></div> </a><div class="col-md-2 col-sm-2" style="padding-left:0px;"><div class="searcharrow col-md-12 col-sm-12" onclick="showdetailsArrowClicked(\'' + "casesbyids" + '\',  \''+resp[i].case_id+'\')"><i class="arrow fa fa-chevron-right " aria-hidden="true" style="font-size:2.1em"></i></div><input class="feedbackcaseid" type="hidden" value="'+resp[i].case_id+'" /><div class="col-md-12 col-sm-12 individual_feedback_container"><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,1)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,1,'+resp[i].case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,2)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,2,'+resp[i].case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,3)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,3,'+resp[i].case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,4)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,4,'+resp[i].case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,5)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,5,'+resp[i].case_id+')" aria-hidden="true"></span></div></div></div></div>';		
			}
			else{
				var card = '<div class="card card-horizontal"><div class="row"><div class="col-md-10 col-sm-10" style="border-right:1px solid #eee;"><div class="content" style="padding-left:10px"><a href="/relation/scr/'+resp[i].case_id+'"><h4 class="title" style="font-weight:bold;">'+resp[i].case_title+'</h4><strong class="title"><b>Judgement - </b>'+resp[i].judgement.substring(0,600)+'...</strong><br/><br/><p class="category">'+resp[i].judge+' '+resp[i].date_of_judgement+'</p> <p class="category">'+resp[i].citation+'</p></div></div> </a><div class="col-md-2 col-sm-2" style="padding-left:0px;"><div class="searcharrow col-md-12 col-sm-12" onclick="showdetailsArrowClicked(\'' + "casesbyids" + '\',  \''+resp[i].case_id+'\')"><i class="arrow fa fa-chevron-right " aria-hidden="true" style="font-size:2.1em"></i></div><input class="feedbackcaseid" type="hidden" value="'+resp[i].case_id+'" /><div class="col-md-12 col-sm-12 individual_feedback_container"><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,1)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,1,'+resp[i].case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,2)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,2,'+resp[i].case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,3)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,3,'+resp[i].case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,4)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,4,'+resp[i].case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,5)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,5,'+resp[i].case_id+')" aria-hidden="true"></span></div></div></div></div>'
			}
			var card = '<div class="card card-horizontal"><div class="row"><div class="col-md-10 col-sm-10" style="border-right:1px solid #eee;"><div class="content" style="padding-left:10px"><a href="/relation/scr/'+resp[i].case_id+'"><h4 class="title" style="font-weight:bold;">'+resp[i].case_title+'</h4><strong class="title"><b>Subject - </b>'+resp[i].subject.substring(0,300)+'...</strong><br/><br/><p class="description"><b>Head Note - </b>'+resp[i].headnote.substring(0,300)+'...</p><p class="category">'+resp[i].judge+' '+resp[i].date_of_judgement+'</p> <p class="category">'+resp[i].citation+'</p></div></div> </a><div class="col-md-2 col-sm-2" style="padding-left:0px;"><div class="searcharrow col-md-12 col-sm-12" onclick="showdetailsArrowClicked(\'' + "casesbyids" + '\',  \''+resp[i].case_id+'\')"><i class="arrow fa fa-chevron-right " aria-hidden="true" style="font-size:2.1em"></i></div><input class="feedbackcaseid" type="hidden" value="'+resp[i].case_id+'" /><div class="col-md-12 col-sm-12 individual_feedback_container"><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,1)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,1,'+resp[i].case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,2)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,2,'+resp[i].case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,3)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,3,'+resp[i].case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,4)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,4,'+resp[i].case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,5)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,5,'+resp[i].case_id+')" aria-hidden="true"></span></div></div></div></div>'
			temp += card;
		}
		showdetailsArrowClicked("casesbyids", resp[from].case_id);
	}
	else{
		for(var i=from;i<from+offset;i++){		
			
			if(resp[i]._source.subject.length > 20 && resp[i]._source.headnote.length > 20 ){
				var card = '<div class="card card-horizontal"><div class="row"><div class="col-md-10 col-sm-10" style="border-right:1px solid #eee;"><div class="content" style="padding-left:10px"><a href="/relation/scr/'+resp[i]._source.case_id+'"><h4 class="title" style="font-weight:bold;">'+resp[i]._source.case_title+'</h4><strong class="title"><b>Subject - </b>'+resp[i]._source.subject.substring(0,300)+'...</strong><br/><br/><p class="description"><b>Head Note - </b>'+resp[i]._source.headnote.substring(0,300)+'...</p><p class="category">'+resp[i]._source.judge+' '+resp[i]._source.date_of_judgement+'</p> <p class="category">'+resp[i]._source.citation+'</p></div></div> </a><div class="col-md-2 col-sm-2" style="padding-left:0px;"><div class="searcharrow col-md-12 col-sm-12" onclick="showdetailsArrowClicked_new(\'' + "casesbyids" + '\',  \''+i+'\')"><i class="arrow fa fa-chevron-right " aria-hidden="true" style="font-size:2.1em"></i></div><input class="feedbackcaseid" type="hidden" value="'+resp[i]._source.case_id+'" /><div class="col-md-12 col-sm-12 individual_feedback_container"><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,1)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,1,'+resp[i]._source.case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,2)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,2,'+resp[i]._source.case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,3)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,3,'+resp[i]._source.case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,4)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,4,'+resp[i]._source.case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,5)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,5,'+resp[i]._source.case_id+')" aria-hidden="true"></span></div></div></div></div>';
			}
			else{				
				var card = '<div class="card card-horizontal"><div class="row"><div class="col-md-10 col-sm-10" style="border-right:1px solid #eee;"><div class="content" style="padding-left:10px"><a href="/relation/scr/'+resp[i]._source.case_id+'"><h4 class="title" style="font-weight:bold;">'+resp[i]._source.case_title+'</h4><strong class="title"><b>Judgement - </b>'+resp[i]._source.judgement.substring(0,600)+'...</strong><br/><br/><p class="category">'+resp[i]._source.judge+' '+resp[i]._source.date_of_judgement+'</p> <p class="category">'+resp[i]._source.citation+'</p></div></div> </a><div class="col-md-2 col-sm-2" style="padding-left:0px;"><div class="searcharrow col-md-12 col-sm-12" onclick="showdetailsArrowClicked_new(\'' + "casesbyids" + '\',  \''+i+'\')"><i class="arrow fa fa-chevron-right " aria-hidden="true" style="font-size:2.1em"></i></div><input class="feedbackcaseid" type="hidden" value="'+resp[i]._source.case_id+'" /><div class="col-md-12 col-sm-12 individual_feedback_container"><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,1)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,1,'+resp[i]._source.case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,2)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,2,'+resp[i]._source.case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,3)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,3,'+resp[i]._source.case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,4)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,4,'+resp[i]._source.case_id+')" aria-hidden="true"></span><span class="glyphicon glyphicon-star-empty" onmouseover="lightupstars(event,5)" onmouseout="dimstars(event)" onclick="giveindividualfeedback(event,5,'+resp[i]._source.case_id+')" aria-hidden="true"></span></div></div></div></div>';
			}
			
			temp += card;
		}
		current_case_title_log = resp[from]._source.case_title;
		current_case_id_log = resp[from]._source.case_id;
		showRightsideDetails("casesbyids", resp[from]._source);
		//showdetailsArrowClicked("casesbyids", resp[from]._source.case_id);
	}
	
	
	from += offset;   //important to be placed here!
	$("#srp").append(temp);
	//$("#loader").hide();
	$('.spinner').hide();
	$(".resultDiv").show();
	$(".casecontent").show();
	$("#loadmorebutton").show();
	$("#feedbackbutton").show();
}

function giveindividualfeedback(e,rating,feedbackcid)
{
	if(rating == 1)
	{
		$(e.target).closest('.card-horizontal').hide(500,function(){
			$(e.target).closest('.card-horizontal').remove();
			if(feedbackcid == current_case_id_log)
			{
				var v = $('#srp').find('.feedbackcaseid:lt(1)').val();
				$('#mCSB_1_container').fadeOut(500,function(){
					$('#mCSB_1_container').html('');
				   showdetailsArrowClicked("casesbyids", v);
				   $('#mCSB_1_container').show(1000);
				});

			}
		});


	}
	else
	{
		 $(e.target).closest('.individual_feedback_container').html("<b>Thank You for your Feedback!</b><br/><span class='glyphicon glyphicon-ok' style='font-size: 2.5vw !important;' aria-hidden='true'></span>");
	}
	//write code to send values to the backend
	var body = {"search_id":search_id,"case_id":feedbackcid,"rating":rating};
	var ajaxbody = {
		url: "case_feedback",
		body: JSON.stringify(body)
	}
	
	$.ajax({
		type:'POST',
		url: "/ajax",
		data:ajaxbody,
		dataType: "json"
	}).done(function(resp){
		
	}).error(function(err){
		
	})
}

//global variable to capture overall rating
var overallrating = 0;
function lightoverallfeedback(e,rating)
{
	$(e.target).closest('.individual_feedback_container').find("span:lt("+rating+")").removeClass("glyphicon-star-empty");
	$(e.target).closest('.individual_feedback_container').find("span:lt("+rating+")").addClass("glyphicon-star");
	$(e.target).closest('.individual_feedback_container').find("span:gt("+parseInt(rating - 1)+")").removeClass("glyphicon-star");
	$(e.target).closest('.individual_feedback_container').find("span:gt("+parseInt(rating - 1)+")").addClass("glyphicon-star-empty");
	overallrating = rating;
}

function giveoverallfeedback(e)
{
	if(overallrating == 0)
	{
		alert("The feedback rating cannot be 0. Please select atleast 1 star");
		return;
	}
	var feedbacktext = $.trim($('#comment').val());
	$(e.target).closest('.individual_feedback_container').html("<h4 style='color:#000;'>Thank You for your Feedback!</h4><span class='glyphicon glyphicon-ok' style='color:#000;font-size: 4vw !important;' aria-hidden='true'></span>");
	//code to send values to the backend
	var body = {"search_id":search_id,"rating":overallrating,"feedback":feedbacktext};
	var ajaxbody = {
		url: "search_result_feedback",
		body: JSON.stringify(body)
	}
	
	$.ajax({
		type:'POST',
		url: "/ajax",
		data:ajaxbody,
		dataType: "json"
	}).done(function(resp){
		
	}).error(function(err){
		console.log("error - "+err);
	})
}

function lightupstars(e,starval)
{
	$(e.target).closest('.individual_feedback_container').find("span:lt("+starval+")").removeClass("glyphicon-star-empty");
	$(e.target).closest('.individual_feedback_container').find("span:lt("+starval+")").addClass("glyphicon-star");
}

function dimstars(e)
{
   $(e.target).closest('.individual_feedback_container').find("span").removeClass("glyphicon-star");
   $(e.target).closest('.individual_feedback_container').find("span").addClass("glyphicon-star-empty");
}

function showdetailsArrowClicked(url, caseid){
	var req_url =url ;
	showmedetails(req_url, caseid, function(resp){
		// added new variables for case title and case id
		if(req_url == "casesbyids"){
			current_case_title_log = resp[0].case_title;
			current_case_id_log = resp[0].case_id;
			showRightsideDetails(url, resp[0]);
			}
		else if (req_url == 'casesby_delhi_ids'){
			current_case_title_log = resp[0].case_title;
			current_case_id_log = resp[0].case_id;
			showRightsideDetails(url, resp[0]);
		}
		else if (req_url == 'casesby_mumbai_ids'){
			current_case_title_log = resp[0].case_title;
			current_case_id_log = resp[0].case_id;
			showRightsideDetails(url, resp[0]);
		}
		else if (req_url == 'cases_by_us_ids'){
			
			current_case_title_log = resp.case_title;
			current_case_id_log = resp.case_id;
			showRightsideDetails(url, resp);
		}
		else{
			
			current_case_title_log = resp[0].title;
			current_case_id_log = resp[0].corpus_id;
			showRightsideDetails(url, resp[0]);
		}
	})
}

function showdetailsArrowClicked_new(url, index){
	
	if(url=='casesbyids'){
		current_case_title_log = cases[index]._source.case_title;
		current_case_id_log = cases[index]._source.case_id;
		showRightsideDetails("casesbyids", cases[index]._source);	
	}
	else{
		var searchType = localStorage.getItem("bymachine");
		if("" + searchType + "" == "true"){
			current_case_title_log = cases[index].case_title;
			current_case_id_log = cases[index].case_id_source;
			showRightsideDetails("cases_by_us_ids", cases[index]);	
		}
		else{
			current_case_title_log = cases[index]._source.case_title;
			current_case_id_log = cases[index]._source.case_id_source;
			showRightsideDetails("cases_by_us_ids", cases[index]._source);		
		}
		
	}
}

function saveContent(context) {
	var ui = $.summernote.ui;
	var button = ui.button({
		contents: '<i class="fa fa-save"/> Save',
		tooltip: 'Save',
		click: function () {
			var content  = "";
			var pageName = "";
			var tag = "";
			if(window.location.pathname =="/notePad"){
				content = $("#notePad").summernote("code");
				pageName = "notePad"
				swal({
					title: 'Tag your research note',
					input: 'text',
					showCancelButton: true,
					confirmButtonText: 'Submit',
					showLoaderOnConfirm: true,
					allowOutsideClick: false
				}).then(function (text) {
					var postData = {
						content  : content,
						pageName : pageName,
						tag      : text
					};
					$.ajax({
						type:'POST',
						url: '/saveNote',
						data: postData,
						dataType: "json"
					}).done(function(resp){
						swal({
							type: 'success',
							title: 'Saved!',
							html: 'Saved research note: ' + text
						});
					}).fail(function(resp){
						swal({
							type: 'error',
							title: 'Try again!',
							html: 'Error saving research note'
						});
					});
					
				})	
			}
			else if(window.location.pathname =="/synopsis"){
				content = $("#summernote").summernote("code");
				pageName = "synopsis";
				swal({
					title: 'Tag your note',
					input: 'text',
					showCancelButton: true,
					confirmButtonText: 'Submit',
					showLoaderOnConfirm: true,
					allowOutsideClick: false
				}).then(function (text) {
					var postData = {
						content  : content,
						pageName : pageName,
						tag      : text
					};
					$.ajax({
						type:'POST',
						url: '/saveNote',
						data: postData,
						dataType: "json"
					}).done(function(resp){
						swal({
							type: 'success',
							title: 'Saved!',
							html: 'Submitted note: ' + text
						});
					});
					
				});
			}
			else if(window.location.pathname.split('/')[1] =="loadNote"){
				content = $("#loadNote").summernote("code");
				pageName = "loadNote";
				tag = window.location.pathname.split('/')[2];	
				var postData = {
					content  : content,
					pageName : pageName,
					tag      : tag
				};
				$.ajax({
					type:'POST',
					url: '/saveNote',
					data: postData,
					dataType: "json"
				}).done(function(resp){
					swal({
						type: 'success',
						title: 'Saved!',
						html: 'Submitted note: ' + tag
					});
				});
					
				
			}
			
			//context.invoke('editor.insertText', 'hello');
		}
	});
	return button.render();   // return button as jquery object
}

function saveNote(){
	
	swal('Any fool can use a computer');
	/*var savePopup = '<div id="savePopup"><form class="form" action="#" id="contact"><br/><label>Name: <span>*</span></label><br/><input type="text" id="name" placeholder="Name"/><br/><br/></div>';
	$("#savePopup").dialog();*/
}

// functions to process data and generate html for synopsis page start
function synopsisdata(flag){
	if(flag==true){
		
		$("#summernote").summernote('reset');
	}
	$("#summernote").summernote({height: 500,focus: true,
				toolbar: [
							  ['style', ['bold', 'italic', 'underline', 'clear']],
							  ['font', ['strikethrough', 'superscript', 'subscript']],
							  ['fontsize', ['fontsize']],
							  ['color', ['color']],
							  ['para', ['ul', 'ol', 'paragraph']],
							  ['misc', ['print']],
							  ['save', ['save']],
							  ['insert', ['share']]
							],
				buttons: {
							save: saveContent
						 },
				share: {
					path: '/js/tpls',
					list: {
						'label-gmail':'Gmail',
						'label-linkedin':'LinkedIn',
						'label-whatsapp':'WhatsApp'
					}
				}

	});
	var synopsisarraysorting = JSON.parse(localStorage.getItem('synopsisarray'));
	
	synopsisarraysorting.sort(function(a,b){
		if(parseInt(a.id) >= parseInt(b.id))
		{
			return 1;
		}
		else
		{
			return -1;
		}
	});
	var html = "";
	var tempid = "";
	var case_type_us = localStorage.getItem('case_type_us');

	if("" + case_type_us + "" == "true"){
		for(var i = 0; i < synopsisarraysorting.length;i++)
		{
			if(tempid != synopsisarraysorting[i].id)
			{
				//html+='<br/><b>'+synopsisarraysorting[i].title+'</b>';
				html+='<br/><a class="btn btn-primary" data-toggle="collapse" href="#casedata'+synopsisarraysorting[i].id+'" onclick=showCaseDetailsSynopsis_us('+synopsisarraysorting[i].id+') aria-expanded="false" aria-controls="collapseExample" style="width:100%;border:0px;text-align:left;padding-left: 0px;color: #054c82;font-size:16px;" contenteditable="false"><b>'+synopsisarraysorting[i].title+'</b></a>';
				html+='<div id="casedatacontainer'+synopsisarraysorting[i].id+'" style="background:none 0px 0px repeat scroll rgb(228, 247, 247);color:#fff;max-height:250px !important;overflow:auto;padding:10px;display:none;"><div class="collapse" id="casedata'+synopsisarraysorting[i].id+'"></div></div>';
				tempid = synopsisarraysorting[i].id;
			}
			html+='<li style="margin-left: 30px;color:#042f4f" contenteditable="false">'+synopsisarraysorting[i].note+'</li>';
		}	
	}
	else{
		for(var i = 0; i < synopsisarraysorting.length;i++)
		{
			if(tempid != synopsisarraysorting[i].id)
			{
				//html+='<br/><b>'+synopsisarraysorting[i].title+'</b>';
				html+='<br/><a class="btn btn-primary" data-toggle="collapse" href="#casedata'+synopsisarraysorting[i].id+'" onclick=showCaseDetailsSynopsis('+synopsisarraysorting[i].id+') aria-expanded="false" aria-controls="collapseExample" style="width:100%;border:0px;text-align:left;padding-left: 0px;color: #054c82;font-size:16px;" contenteditable="false"><b>'+synopsisarraysorting[i].title+'</b></a>';
				html+='<div id="casedatacontainer'+synopsisarraysorting[i].id+'" style="background:none 0px 0px repeat scroll rgb(228, 247, 247);color:#fff;max-height:250px !important;overflow:auto;padding:10px;display:none;"><div class="collapse" id="casedata'+synopsisarraysorting[i].id+'"></div></div>';
				tempid = synopsisarraysorting[i].id;
			}
			html+='<li style="margin-left: 30px;color:#042f4f" contenteditable="false">'+synopsisarraysorting[i].note+'</li>';
		}	
	}
	
	//console.log(html);
	//$("#research_notes_wrap").html(html);
	
	$('#summernote').summernote('pasteHTML',html);


}

//function to evaluate the synopsis (or summariser)
function evaluateSynopsis(){
	research_notes = $("#research_notes_wrap").text();
	body = {"notes" :research_notes};
	var ajaxbody = {
		url: 'summarise',
		body: JSON.stringify(body)
	}

	$.ajax({
		type:'POST',
		url: '/summarise',
		data:ajaxbody,
		dataType: "json"
	}).done(function(resp){
		if(resp.hasOwnProperty('error'))
		{

		}
		else
		{
			summary = resp.summary;
			$("#synopsisnotes").html(summary);
			
			

		}

		return resp;
	}).error(function(err){
		
		return err;
	})

}


var tooltip,hidetooltiptimer;

		function createtooltip() { // call this function ONCE at the end of page to create tool tip object
			tooltip = document.createElement('div')
			tooltip.setAttribute('id', "copy_tooltip");
			tooltip.style.cssText =
				'position:absolute; background:#042F4F; color:white; padding:10px;z-index:10000;'
				+ 'border-radius:2px; font-size:12px;box-shadow:3px 3px 3px rgba(0,0,0,.4);'
				+ 'opacity:0;transition:opacity 0.3s'
			tooltip.innerHTML = 'Copy to research notes >'
			document.body.appendChild(tooltip)
		}

		function showtooltip(e) {
			var evt = e || event
			/*clearTimeout(hidetooltiptimer);*/
			tooltip.style.left = evt.pageX - 10 + 'px';
			tooltip.style.top = evt.pageY + 15 + 'px';
			tooltip.style.opacity = 1;
			/*
			 hidetooltiptimer = setTimeout(function () {
			 tooltip.style.opacity = 0
			 }, 500);
			 */
		}

function showCaseDetailsSynopsis(caseid){
	
	showmedetails("casesbyids", caseid, function(resp){
	
		caseDetails = resp[0];
		casetitle = caseDetails.case_title;

		localStorage.setItem("current_research_synopsis_title_copied", casetitle);
		
		localStorage.setItem("current_research_synopsis_id_copied", caseid);
		htmlData        = "<p class='title' style='font-size:13px;'><strong >Subject :</strong> "+caseDetails.subject+"</p><p><strong>Citation : </strong>"+caseDetails.citation+"</p><p class='category' style='font-size:13px;'><strong>Judge :</strong> "+caseDetails.judge+"</p><p class='category' style='font-size:13px;'><strong>Date of Judgement :</strong> "+caseDetails.date_of_judgement+"</p><p style='font-size:13px;'><strong>Full Judgement :</strong> <br>"+caseDetails.judgement+"</p>";
		//displayvalue = document.getElementById('casedatacontainer'+caseid),
		  displayvalue = document.getElementById('casedatacontainer'+caseid).style.display;
		if(displayvalue == 'none') {
			document.getElementById('casedatacontainer' + caseid).style.display = "block";
		}else{
			document.getElementById('casedatacontainer' + caseid).style.display = "none";
		}
		/*if($("#casedatacontainer"+caseid).hasClass('customheight'))
		{
			$("#casedatacontainer"+caseid).removeClass('customheight');
		}
		else
		{
			$("#casedatacontainer"+caseid).addClass('customheight');
		}*/
		$("#casedata"+caseid).html(htmlData); //load new content inside .mCSB_container
		//$("#casedata"+caseid).summernote('pasteHTML',htmlData);
		//$('#summernote').summernote('pasteHTML',htmlData);
		var documentid = 'casedata'+caseid;
		
		var buddhaquote = document.getElementById(documentid);
		var text = "";
		buddhaquote.addEventListener('mouseup', function (e) {
			
			if (window.getSelection) {
				text = window.getSelection().toString();
			} else if (document.selection && document.selection.type != "Control") {
				text = document.selection.createRange().text;
			}
			//if (text != "") {
			{
				//createsynopsisarray(current_case_title_log,text,current_case_id_log);
				localStorage.setItem("current_research_synopsis_notes_copied", text);		
				showtooltip(e)
			}

		}, false)
	})
}

function showCaseDetailsSynopsis_us(caseid){
	
	showmedetails("cases_by_us_ids", caseid, function(resp){
	
		caseDetails = resp[0];
		casetitle = caseDetails.case_title;

		localStorage.setItem("current_research_synopsis_title_copied", casetitle);
		
		localStorage.setItem("current_research_synopsis_id_copied", caseid);
		if(caseDetails.html_with_citations.length!=0){
			htmlData = caseDetails.html_with_citations;	
		}
		else if(caseDetails.html.length!=0){
			htmlData = caseDetails.html;
		}
		else if(caseDetails.html_lawbox!=0){
			htmlData = caseDetails.html_lawbox;
		}	
		
		displayvalue = document.getElementById('casedatacontainer'+caseid).style.display;
		if(displayvalue == 'none') {
			document.getElementById('casedatacontainer' + caseid).style.display = "block";
		}else{
			document.getElementById('casedatacontainer' + caseid).style.display = "none";
		}
		
		$("#casedata"+caseid).html(htmlData); //load new content inside .mCSB_container
		
		var documentid = 'casedata'+caseid;
		
		var buddhaquote = document.getElementById(documentid);
		var text = "";
		buddhaquote.addEventListener('mouseup', function (e) {
			if (window.getSelection) {
				text = window.getSelection().toString();
			} else if (document.selection && document.selection.type != "Control") {
				text = document.selection.createRange().text;
			}
			localStorage.setItem("current_research_synopsis_notes_copied", text);		
			showtooltip(e);
		}, false)
	})
}


// functions to process data and generate html for synopsis page end

function showmedetails(url, caseid,callback){
	var req_url = url ;
	
	var body = {"cases":[parseInt(caseid)]};
	var ajaxbody = {
		url: req_url,
		body: JSON.stringify(body)
	}
	
	$.ajax({
		type:'POST',
		url: "/ajax",
		data:ajaxbody,
		dataType: "json"
	}).done(function(resp){
		
		callback(resp);
		return resp;
	}).error(function(err){
		
		return err;
	})
}

function showRightsideDetails(url, caseDetails){
	console.log(url);
	console.log(caseDetails);
	if (url == "casesbyids"){
		  var htmlData        = "<h4> "+caseDetails.case_title+"</h4><p class='title'><strong >Subject :</strong> "+caseDetails.subject+"</p><p><strong>Citation : </strong>"+caseDetails.citation+"</p><p class='category'><strong>Judge :</strong> "+caseDetails.judge+"</p><p class='category'><strong>Date of Judgement :</strong> "+caseDetails.date_of_judgement+"</p><p><strong>Full Judgement :</strong> <br>"+caseDetails.judgement+"</p>";
		  $("#mCSB_1_container").html(htmlData); //load new content inside .mCSB_container
	 }
	 else if (url == "casesby_delhi_ids"){
		var htmlData        = "<h4> "+caseDetails.case_title+"</h4><p class='title'><strong >Case Name :</strong> "+caseDetails.case_name+"</p><p class='category'><strong>Judge :</strong> "+caseDetails.judge+"</p><p class='category'><strong>Date of Judgement :</strong> "+caseDetails.date_of_judgement+"</p><p><strong>Full Judgement :</strong> <br>"+caseDetails.judgement+"</p>";
		  $("#mCSB_1_container").html(htmlData); //load new content inside .mCSB_container
	 }

	 else if (url == "casesby_mumbai_ids"){
		var htmlData        = "<h4> "+caseDetails.case_title+"</h4><p class='title'><strong >Case Name :</strong> "+caseDetails.case_name+"</p><p class='category'><strong>Judge :</strong> "+caseDetails.judge+"</p><p class='category'><strong>Date of Judgement :</strong> "+caseDetails.date_of_judgement+"</p><p><strong>Full Judgement :</strong> <br>"+caseDetails.judgement+"</p>";
		  $("#mCSB_1_container").html(htmlData); //load new content inside .mCSB_container
	 }

	 else if(url == "cases_by_us_ids"){

		if(caseDetails.html_with_citations.length!=0){
			var usJudgement = caseDetails.html_with_citations;	
		}
		else if(caseDetails.html.length!=0){
			var usJudgement = caseDetails.html;
		}
		else if(caseDetails.html_lawbox!=0){
			var usJudgement = caseDetails.html_lawbox;
		}
		else{
			var usJudgement = caseDetails.plain_text;
		}
	 	var htmlData        = "<p>"+usJudgement+"</p>";
		$("#mCSB_1_container").html(htmlData); //load new content inside .mCSB_container	
	 }
	 else{
	  var htmlData = "<h4> "+caseDetails.title+"</h4><p class='title'><strong >Subject :</strong> "+caseDetails.tax_law_type.substring(0,300)+ "</p><p class='category'><strong>Date of Judgement :</strong> "+caseDetails.date+"</p><p><strong>Full Judgement :</strong> <br>"+caseDetails.judgement+"</p>";
	  $("#mCSB_1_container").html(htmlData); //load new content inside .mCSB_container
	 }
}


function getSimilarcases(){
	var case_id = $("#caseid").text();
	var data_type = $('#data_type').text();
	
	similarity_url = "similarbycaseid";
	fetch_case_url = "casebyids";
	if(data_type == "tax" ){
		similarity_url = "tax_similarbycaseid";
		fetch_case_url = "casesby_tax_ids";
	}
	else if (data_type == "delhi"){
		similarity_url = "delhi_similarbycaseid" ;
		fetch_case_url = "casesby_delhi_ids";
	}
	else if (data_type == "mumbai"){
		similarity_url = "mum_similarbycaseid" ;
		fetch_case_url = "casesby_mumbai_ids" ;
	}
	else if (data_type == "us"){
		similarity_url = "us_similarbycaseid" ;
		fetch_case_url = "casesby_us_ids" ;
	}
	else{
		similarity_url = "similarbycaseid" ;
		fetch_case_url = "casebyids";
	}
	var body = {"case_id":parseInt(case_id)};
	var ajaxbody = {
		url: similarity_url,
		body: JSON.stringify(body)
	}
	localStorage.setItem("current_case_id",case_id);
	$.ajax({
		type:'POST',
		url: "/ajax",
		data:ajaxbody,
		dataType: "json"
	}).done(function(resp){
		console.log(resp);
		from = 0;
		cases = resp.similar_cases;
		if(fetch_case_url!='casesby_us_ids'){
			showmedetails(fetch_case_url, resp.request_case,function(resp){
				placeInMainCircles(resp[0])
			});
			creategraphcases(cases,$('#slider').val());
		}
		else{
			placeInMainCircles(resp.request_case);
			creategraphcases_us(resp.request_case,cases,$('#slider').val());
		}
		

		return resp;
	}).error(function(err){
		
		return err;
	})
}

function placeInMainCircles(mainCase){

	localStorage.setItem("current_case_title",mainCase.case_title);
	$("#maincase").text(mainCase.case_title);
	$("#maincase1").text(mainCase.case_title);
}

function placeInCircles(resp){
	// $("#srp").html("");
	var temp = "";
	$.each(resp, function(key, value){
		if(key < 8){
			var circle = '<div class="option-A circle-'+parseInt(key+1)+'" data-caseid="'+value.case_id+'"><div class="circle">'+value.case_title.substring(0,65)+'..</div><hr class="line-'+parseInt(key+1)+'"></div>'
		}
		temp += circle;
	})
	$("#circles").after(temp);

}



function SeeCaseNotes(){
	localStorage.setItem("cases",selectedcases);
	window.location.href = '/case';
}

function compareTheseCases(){
	cases = localStorage.getItem("cases",selectedcases);
	cases = cases.split(",");
	
	getDetailData("casesbyids", function(resp){
		placeInComparingCircles(resp)
	});
}


function BackToRelationPage() {
	var current_case_id = localStorage.getItem("current_case_id");
	window.location.href = '/relation/'+current_case_id;

}

function placeInComparingCircles(resp){
	
	var temp = "";
	$.each(resp, function(key, value){
		if(key < 8){
			var circle = '<div class="field" data-caseid="'+value.case_title+'" style="left: 89px; top: 19px;" onclick="showdetailsArrowClicked(\'casesbyids\', \''+value.case_id+'\')">'+value.case_title.substring(0,20)+'..</div>'
		}
		temp += circle;
		showdetailsArrowClicked("casesbyids", resp[0].case_id);
	})

	$("#circles").after(temp);
	var radius = 90;
	var fields = $('.field'), container = $('#shotlistwrapper'), width = container.width(), height = container.height();
	var angle = 0, step = (2*Math.PI) / fields.length;
	fields.each(function() {
		var x = Math.round(width/2 + radius * Math.cos(angle) - $(this).width()/2);
		var y = Math.round(height/2 + radius * Math.sin(angle) - $(this).height()/2);
		if(window.console) {
			// console.log($(this).text(), x, y);
		}
		$(this).css({
			left: x + 'px',
			top: y + 'px'
		});
		angle += step;
	});
}

// function to create data array for synopsis page
function createsynopsisarray(synopsistitle,synopsisnote,synopsisid){
	
	var nodeobject = {title:synopsistitle, note:synopsisnote,id:synopsisid};
	synopsisarray.push(nodeobject);
	localStorage.setItem("synopsisarray",JSON.stringify(synopsisarray));
}

function creategraphcases(caseids, sliderval) {
	$('#graphcontainer').html('');

	//getting the caseids as per the value of the slider
	var presentcases = [];
	var min = 0;
	if((parseInt(sliderval) + 1) < caseids.length)
	{
		min = parseInt(sliderval) + 1;
	}
	else
	{
		min = caseids.length;
	}
	for(i = 0; i < 11; i++){
		if(caseids[i].case_id != undefined && caseids[i].case_id != null){
			presentcases.push(caseids[i].case_id)
		}else{
			presentcases.push(parseInt(caseids[i]))
		}
	}
	var body = {"cases":presentcases};
	var ajaxbody = {
		url: "casesbyids",
		body: JSON.stringify(body)
	}
	
	$.ajax({
		type:'POST',
		url: "/ajax",
		data:ajaxbody,
		dataType: "json"
	}).done(function(resp){

		//on getting the response, finding the main case in the response and setting it as the first element in the response
		var maincaseval = 0;
		/*for(var i = 1;i<resp.length;i++)
		{
			if(resp[i].case_id == $('#caseid').text())
			{
				maincaseval = i;
				localStorage.setItem("current_case_title", resp[i].case_title);
			}
		}*/
		$('.spinner').hide();
		$('.relationWrapper').show();
		//$('.bottomRow').show();
		var splicedval = resp[maincaseval];
		resp.splice(maincaseval,1);
		resp.unshift(splicedval);
		
		var caseDetails = resp[0];
		var caseString = JSON.stringify(caseDetails);
		var temp = "<div class='recommendationCard defaultCase'>"+resp[0].case_title+"</div>";
		temp+="<div class='recommendationTitle'>Related Cases</div>"
		localStorage.setItem("current_case_title",caseDetails.case_title);
		for(var i=1; i<11; i++){
			caseString = JSON.stringify(resp[i]);
			var card = "<div class='recommendationCard'>"+ resp[i].case_title + "</div>";
			temp+=card;
		}
		$(".caseRecommendations").append(temp);
		var htmlData = "<h4> "+caseDetails.case_title+"</h4><p class='title'><strong >Subject :</strong> "+caseDetails.subject+"</p><p><strong>Citation : </strong>"+caseDetails.citation+"</p><p class='category'><strong>Judge :</strong> "+caseDetails.judge+"</p><p class='category'><strong>Date of Judgement :</strong> "+caseDetails.date_of_judgement+"</p><p><strong>Full Judgement :</strong> <br>"+caseDetails.judgement+"</p>";
		$(".caseDetails").html(htmlData); //load new content inside .mCSB_container
		//creategraph(resp,sliderval);
		$(".recommendationCard").on("click",function(){
			$(".recommendationCard").removeClass("defaultCase");
			$(this).addClass("defaultCase");
			caseDetails = resp[$(".recommendationCard").index(this)];
			var htmlData = "<h4> "+caseDetails.case_title+"</h4><p class='title'><strong >Subject :</strong> "+caseDetails.subject+"</p><p><strong>Citation : </strong>"+caseDetails.citation+"</p><p class='category'><strong>Judge :</strong> "+caseDetails.judge+"</p><p class='category'><strong>Date of Judgement :</strong> "+caseDetails.date_of_judgement+"</p><p><strong>Full Judgement :</strong> <br>"+caseDetails.judgement+"</p>";
			$(".caseDetails").html(htmlData); //load new content inside .mCSB_container
			localStorage.setItem("current_case_title",caseDetails.case_title);
			localStorage.setItem("case_id",caseDetails.case_id);
			localStorage.setItem("current_case_id",caseDetails.case_id);
		})


	}).error(function(err){
		
		return err;
	})
}

function creategraphcases_us(request_case,cases, sliderval) {
		$('#graphcontainer').html('');
		$('.spinner').hide();
		$('.relationWrapper').show();
		console.log('1');
		var caseDetails = '';
		if(request_case.html_with_citations!=''){
			caseDetails = request_case.html_with_citations;
		}
		else if(request_case.html!=''){
			caseDetails = request_case.html;
		}
		else if(request_case.html_lawbox!=''){
			caseDetails = request_case.html_lawbox;	
		}
		
		var temp = "<div class='recommendationCard defaultCase'>"+request_case.case_title+"</div>";
		temp+="<div class='recommendationTitle'>Related Cases</div>"
		localStorage.setItem("current_case_title",request_case.case_title);
		localStorage.setItem("case_id",request_case.case_id_source);
		localStorage.setItem("current_case_id",request_case.case_id_source);
		localStorage.setItem("case_type_us",true);
		for(var i=0; i<11; i++){
			var card = "<div class='recommendationCard'>"+ cases[i].case_title + "</div>";
			temp+=card;
		}
		console.log('2');
		$(".caseRecommendations").append(temp);
		
		$(".caseDetails").html(caseDetails); //load new content inside .mCSB_container
		console.log('3');
		$(".recommendationCard").on("click",function(){
			$(".recommendationCard").removeClass("defaultCase");
			$(this).addClass("defaultCase");
			var selectedCase = cases[$(".recommendationCard").index(this)];
			if(selectedCase.html_with_citations!=''){
				caseDetails = selectedCase.html_with_citations;
			}
			else if(selectedCase.html!=''){
				caseDetails = selectedCase.html;
			}
			else if(selectedCase.html_lawbox!=''){
				caseDetails = selectedCase.html_lawbox;	
			}
			$(".caseDetails").html(caseDetails); //load new content inside .mCSB_container
			localStorage.setItem("current_case_title",selectedCase.case_title);
			localStorage.setItem("case_id",selectedCase.case_id_source);
			localStorage.setItem("current_case_id",selectedCase.case_id_source);
		})
}



function selectAsDefault(caseDetails)
{
	
	$(".recommendationCard").removeClass("defaultCase");
	
	$(caseDetails).addClass("defaultCase");
	/*var htmlData = "<h4> "+caseDetails.case_title+"</h4><p class='title'><strong >Subject :</strong> "+caseDetails.subject+"</p><p><strong>Citation : </strong>"+caseDetails.citation+"</p><p class='category'><strong>Judge :</strong> "+caseDetails.judge+"</p><p class='category'><strong>Date of Judgement :</strong> "+caseDetails.date_of_judgement+"</p><p><strong>Full Judgement :</strong> <br>"+caseDetails.judgement+"</p>";
	$(".caseDetails").html(htmlData); //load new content inside .mCSB_container*/
	
}

function creategraph(dataArray,sliderval)
{
	$('.loader').show();
	$("#graphcontainer").html('');

	//For Tooltip
	//$("#tooltipcontainer").html('');

	var width = $("#graphcontainer").width();
	var height = $("#graphcontainer").height();

	/* For Tooltip
	var div = d3.select("#tooltipcontainer").append("div")
		.attr("class", "tooltip")
		.style("opacity", 0);
	*/


	//adding main case i.e first node in the response as the central node
	var nodes = [{
		x: width / 3,
		y: height / 3,
		case_id: dataArray[0].case_id,
		case_title: dataArray[0].case_title,
		citation:dataArray[0].citation,
		date_of_judgement:dataArray[0].date_of_judgement,
		headnote: dataArray[0].headnote,
		judge: dataArray[0].judge,
		judgement: dataArray[0].judgement,
		subject: dataArray[0].subject
	}];

	//setting icrements as per value of the slider
	var increment = 360 / sliderval;
	var counter = 1;
	for (var angle = 0; angle < 360; angle = angle + increment) {
		//setting and creating additional similar cases nodes
		var nodeObject = {
			x: (width / 3) + (width / 4) * Math.cos(angle / 180 * Math.PI),
			y: (height / 3) + (height / 4) * Math.sin(angle / 180 * Math.PI),
			case_id: dataArray[counter].case_id,
			case_title: dataArray[counter].case_title,
			citation:dataArray[counter].citation,
			date_of_judgement:dataArray[counter].date_of_judgement,
			headnote: dataArray[counter].headnote,
			judge: dataArray[counter].judge,
			judgement: dataArray[counter].judgement,
			subject: dataArray[counter].subject
		};
		nodes.push(nodeObject);
		counter++;
	}

	//creating links from central node to all other nodes
	var links = [];
	for (var i = 1; i < nodes.length; i++) {
		var linkObject = {
			source: 0,
			target: i
		};
		links.push(linkObject);
	}

	var svg = d3.select('#graphcontainer').append('svg')
		.attr('width', width)
		.attr('height', height)
		.attr('style',  'background-color:#f9fcff');

	var force = d3.layout.force()
		.size([width, height])
		.nodes(nodes)
		.links(links)
		.start();


	var link = svg.selectAll('.link')
		.data(links)
		.enter().append('line')
		.attr('class', 'link');


	var node = svg.selectAll(".node")
		.data(nodes)
		.enter().append("g")
		.attr("class", "node");

	var radius = width / 14;
	node.append("circle")
		.attr('r', radius)
		.attr('cx', function(d) {
			return d.x;
		})
		.attr('cy', function(d) {
			return d.y;
		})

	var side = 2 * radius * Math.cos(Math.PI / 4)
	var opacity_factor = 0;
	node.append('foreignObject')
		.attr('x', function(d) {
			return d.x - (side / 2)
		})
		.attr('y', function(d) {
			return d.y - (side / 2)
		})
		.attr('width', side)
		.attr('height', side)
		.attr('color', 'black')
		.append('xhtml:p')
		.html(function(d) {
			return '<a style="cursor:pointer;" data-toggle="modal" data-target=".bs-example-modal-lg">' + d.case_title + '</a>'
		})
		.attr('style', 'text-align:center;font-size:9px;overflow:hidden')
		.on("click", function(d) {
			/* For Custom Tooltip
			var opacity_factor = 0;
			if (div.style('opacity') == 0) {
				opacity_factor = 1;
			} else {
				opacity_factor = 0;
			}
			div.transition()
				.duration(300)
				.style("opacity", opacity_factor * 0.9);
			div.html("<div style='height:100px;overflow:auto;'><div class='col-md-12' style='border-bottom:1px solid #fff;padding:10px;'><b>" + d.case_title + "</b></div><div class='col-md-12' style='border-bottom:1px solid #fff;padding:10px;'>" + d.headnote + "</div></div><div class='col-md-6' style='padding:10px;'><button class='btn btn-info btn-fill btn-block' onclick='addToNotesRelationPage(\""+d.case_title+"\","+d.case_id+")'>Add to notes</button></div><div class='col-md-6' style='padding:10px;'><button class='btn btn-info btn-block btn-fill' onclick='selectasmaincase(\""+d.case_title+"\","+d.case_id+")'>Select as main</button></div>")
				.style("left", (d3.event.pageX + 10) + "px")
				.style("top", (d3.event.pageY - 40) + "px");
			*/

			//Display selected case data in a predefined modal with buttons for adding to notes and selecting as the main case
			$('.modal-header').html('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">�</span></button><h4><b class="modal-title" id="myModalLabel">'+d.case_title+'</b></h4>');
			$('.modal-body').html('<p>'+d.headnote+'</p>');
			$('.modal-footer').html("<div class='col-md-6'><button class='btn btn-info btn-fill btn-block' onclick='addToNotesRelationPage(\""+d.case_title+"\","+d.case_id+")'>Add to notes</button></div><div class='col-md-6'><button class='btn btn-info btn-block btn-fill' onclick='selectasmaincase(\""+d.case_title+"\","+d.case_id+")'>Select as main</button></div>'");
		});


	link.attr('x1', function(d) {
			return d.source.x;
		})
		.attr('y1', function(d) {
			return d.source.y;
		})
		.attr('x2', function(d) {
			return d.target.x;
		})
		.attr('y2', function(d) {
			return d.target.y;
		});
	$('.loader').hide();

	

}

function selectasmaincase(case_title,case_id)
{
	$('.modal').modal('hide');

	//Taking the old caseid, getting data related to it and populating the history section
	var oldcaseid = $("#caseid").text();
	showmedetails("casesbyids", oldcaseid, function(resp){
		var html = "<ul><li><a style='cursor:pointer;color:#043f4f;font-weight:bold;' onclick='selectasmaincase(\""+resp[0].case_title+"\","+resp[0].case_id+")'>"+resp[0].case_title+"</a></li></ul>";
		$("#history").html($("#history").html() + html);
	})

	//setting the new case id and getting data similar to it
	$("#caseid").html(case_id);
	getSimilarcases();
}

var filter_cases="";
function getsimilarCasesByText() {
	var searchterm = $("#searchterm").text();
	var body = {
		"search_query": searchterm
	};
	var ajaxbody = {
		url: "similartextwordcloud",
		body: JSON.stringify(body)
	}
	$.ajax({
		type: 'POST',
		url: "/ajax",
		data: ajaxbody,
		dataType: "json"
	}).done(function(resp) {
		
		if (resp.error) {
			resp = '';
		} else {
		
			filters = resp.filters;
			filter_cases=resp.filter_cases;
			if (filters.length) {
				var filterData = '';
				var qs = window.location.search;
				var arrQs = [];
				if (qs) {
					qs_val = parseInt(qs.indexOf('=')) + 1;
					qs = qs.substring(qs_val);
					arrQs = qs.split("&");
				}

				$(filters).each(function(index) {
					filter_disp = 1;
					
					filterval = filters[index];
					if (arrQs.length) {
						$(arrQs).each(function(qs_index) {
							if (arrQs[qs_index] == filterval) {
								filter_disp = 0;
							}
						});
					}
					if (filter_disp == 1) {
						filterData += '<li class="' + filterval + '">' + filterval + '<span class="filterdata" data-filter="' + filterval + '">X</span></li>';
					}
				});
				$('.filters').html(filterData);
				//getCases(filter_cases,arrQs);
			}
		}
		return resp;
	}).error(function(err) {
		
		return err;
	})
}

function getsimilarUSCasesByText() {
	var searchterm = $("#searchterm").text();
	var body = {
		"search_query": searchterm
	};
	var ajaxbody = {
		url: "similarustextwordcloud",
		body: JSON.stringify(body)
	}
	$.ajax({
		type: 'POST',
		url: "/ajax",
		data: ajaxbody,
		dataType: "json"
	}).done(function(resp) {
		
		if (resp.error) {
			resp = '';
		} else {
		
			filters = resp.filters;
			filter_cases=resp.filter_cases;
			if (filters.length) {
				var filterData = '';
				var qs = window.location.search;
				var arrQs = [];
				if (qs) {
					qs_val = parseInt(qs.indexOf('=')) + 1;
					qs = qs.substring(qs_val);
					arrQs = qs.split("&");
				}

				$(filters).each(function(index) {
					filter_disp = 1;
					
					filterval = filters[index];
					if (arrQs.length) {
						$(arrQs).each(function(qs_index) {
							if (arrQs[qs_index] == filterval) {
								filter_disp = 0;
							}
						});
					}
					if (filter_disp == 1) {
						filterData += '<li class="' + filterval + '">' + filterval + '<span class="filterdata" data-filter="' + filterval + '">X</span></li>';
					}
				});
				$('.filters').html(filterData);
				//getCases(filter_cases,arrQs);
			}
		}
		return resp;
	}).error(function(err) {
		
		return err;
	})
}



function displayFilteredData() {
	var qs = window.location.search;
	console.log(qs);	
	var arrQs = [];
	if (qs) {
		qs_val = parseInt(qs.indexOf('=')) + 1;
		qs = qs.substring(qs_val);
		arrQs = qs.split("&");
	}
	getCases(filter_cases,arrQs);
}

function displayFilteredData_us(){
	var qs = window.location.search;
	
	var arrQs = [];
	if (qs) {
		qs_val = parseInt(qs.indexOf('=')) + 1;
		qs = qs.substring(qs_val);
		arrQs = qs.split("&");
	}
	getCases(filter_cases,arrQs);
}

function getCases(filter_cases,arrQs) {
	var removeCaseIds=[];
	var workCaseIds=[];    
	cases=[];

	for(filters in filter_cases) {
		var lenFilterdata=filter_cases[filters].length;
		if(jQuery.inArray( filters, arrQs )!="-1") {
			for(var i=0;i<lenFilterdata;i++) {
				if(removeCaseIds.indexOf(filter_cases[filters][i])=="-1") {
					removeCaseIds.push(filter_cases[filters][i]);
				}
			}
		}
		else {
			for(var i=0;i<lenFilterdata;i++) {
				if(workCaseIds.indexOf(filter_cases[filters][i])=="-1") {
					workCaseIds.push(filter_cases[filters][i]);
				}
			}
		}
	}
	
	for(i in workCaseIds) {
		if(removeCaseIds.indexOf(workCaseIds[i])==-1) {
			cases.push(workCaseIds[i]);
		}
	}
	$("#srp").html('');
	$(".casecontent").hide();
	$('.btn-info').hide();
	//alert('cases=='+cases+'---len=='+cases.length);
	if(cases.length) {
		from = 0;
		getDetailData("cases_by_us_ids",function(resp) {
			appendUSResultsHTML(resp);
			$(".casecontent").show();
			$('.btn-info').show();
		});
	}
	else {
		$("#srp").html('No Record Found');
	}
}

