var selectedcases = [];
/* OLD CODE
dragula([document.querySelector('#left-defaults'), document.querySelector('#right-defaults')])
.on('drop', function (el,target) {
    if(selectedcases.indexOf($(el).data("caseid")) == -1){
        selectedcases.push($(el).data("caseid"));
    }else{
        selectedcases.splice(selectedcases.indexOf($(el).data("caseid")), 1);
    }
    console.log(selectedcases);
})
*/
var selectedcasestitles = [];

//function to add case to the notes section
function addToNotesRelationPage(ctitle,cid)
{
    if(selectedcases.indexOf(cid) != -1)
    {
        alert('Case already added to notes');
    }
    else
    {
        $('.modal').modal('hide');
        var html = '<h3 style="text-align:center;font-size:25px">Notes</h3><ul>';
        selectedcases.push(cid);
        selectedcasestitles.push(ctitle);
        console.log(selectedcases);
        for(var i=0;i<selectedcasestitles.length;i++)
        {
            html+= "<li>"+selectedcasestitles[i]+" - <a style='cursor:pointer;' onclick='removeFromNotesrelationPage(\""+selectedcasestitles[i]+"\","+selectedcases[i]+")'>Remove</a></li>";
        }
        html+= "</ul>";
        $('#notes').html(html);
    }
    
}

//function to remove a case from the notes section and repopulate data
function removeFromNotesrelationPage(ctitle,cid)
{
    var html = '<h3 style="text-align:center;font-size:25px">Notes</h3><ul>';
    selectedcases.splice(selectedcases.indexOf(cid),1);
    selectedcasestitles.splice(selectedcasestitles.indexOf(ctitle),1);
    console.log(selectedcases);
    for(var i=0;i<selectedcasestitles.length;i++)
        {
            html+= "<li>"+selectedcasestitles[i]+" - <a style='cursor:pointer;' onclick='removeFromNotesrelationPage(\""+selectedcasestitles[i]+"\","+selectedcases[i]+")'>Remove</a></li>";
        }
    html+= "</ul>";
    $('#notes').html(html);
}