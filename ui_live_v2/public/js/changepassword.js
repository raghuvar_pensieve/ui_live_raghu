var changePasswordApp = angular.module('ui.bootstrap.changepassword', ['ngAnimate', 'ngSanitize', 'ui.bootstrap']);
changePasswordApp.controller('changePasswordController', function($scope, $window, $http) {

    $scope.changepassword = function() {
        if($scope.oldpassword == $scope.reoldpassword)
        {
            $scope.error = "";
            var data =
            JSON.stringify({
                oldpassword: $scope.oldpassword,
                reoldpassword: $scope.reoldpassword,
                newpassword: $scope.newpassword,
            });
            $http.post("/changepasswordvalidate", data).success(function(data, status) {
            if (data == "true") {
                    alert("Your Password has been changed successfully")
                    $window.location.href = '/';
            }
            else {
                $scope.error = data;
            }
        })
        }
        else
        {
            $scope.error = "The passwords don't match";
        }
    }
});