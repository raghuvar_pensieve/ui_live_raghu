var signupApp = angular.module('ui.bootstrap.forgotpassword', ['ngAnimate', 'ngSanitize', 'ui.bootstrap']);
signupApp.controller('forgotpasswordController', function($scope, $window, $http) {

    $scope.forgotpassword = function() {
            $scope.error = "";
            var data =
            JSON.stringify({
                email:$scope.email
            });
            $http.post("/forgotpasswordvalidate", data).success(function(data, status) {
            if (data == "true") {
                    alert("We have sent instructions with the new password on your email ID")
                    $window.location.href = '/';
            }
            else {
                $scope.error = data;
            }
            
        })
        
        
    }
});