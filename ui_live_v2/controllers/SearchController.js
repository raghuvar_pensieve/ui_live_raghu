
function search (req, res, next) {

    // return res.send("WELCOME TO REST API UPDATE");
    var loggedin = false
    if(req.session.username != null)
    {
        loggedin = true
    }
    console.log(req.params);
    res.render('search/search', {
        loggedin:loggedin,
        username:req.session.user,
        title: 'Search',
        searchterm: req.params.searchterm
    })
}


module.exports = {
    search: search
};