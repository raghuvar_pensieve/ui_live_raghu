var request = require('request');
var production = '52.76.250.165';
var staging = '52.220.121.25';
var localhost = 'localhost';
function detail (req, res, next) {
    var loggedin = false
    if(req.session.username == null)
    {
        res.redirect('/login/'+req.params.id)
    }
    else
    {
        loggedin = true

        res.render('relation/detail', {
            loggedin:loggedin,
            username:req.session.user,
            title: 'Relation',
            caseid: req.params.id,
            data_type : req.params.data_type
        });
        var url = 'http://' + production + ':3000/saveCaseId';
        var postData = {
            caseId : req.params.id,
            username : req.session.username,
	        data_type : req.params.data_type
        }
        var options = {
            url: url,
            method: 'post',
            body: postData,
            json: true
        };      
        request(options, function (err, resp, body) {
            if(err) {
                console.log(err);
            } else {
                console.log("relationController line: 37 "+body);
            }
        });

    }
}

function renderCase(req,res,next){
    var loggedin = false
    if(req.session.username == null)
    {
        res.redirect('/login/'+req.params.id)
    }
    else
    {
        loggedin = true
 
        res.render('relation/detail', {
            loggedin:loggedin,
            username:req.session.user,
            title: 'Relation',
            caseid: req.params.id,
            data_type : req.params.data_type
        });
    }   
}

module.exports = {
    detail: detail,
    renderCase: renderCase
};
