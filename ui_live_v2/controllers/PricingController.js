
function pricing (req, res, next) {

    var loggedin = false
    if(req.session.username != null)
    {
        loggedin = true
    }
    res.render('pricing/pricing', {
        loggedin:loggedin,
        username:req.session.user
    });
}


module.exports = {
    pricing: pricing
};