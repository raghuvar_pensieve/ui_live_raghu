var request = require("request");
var util  = require("util");

var staging_url = "52.220.121.25";
var production_url = "52.76.250.165";
var localhost = "localhost"

function suggest (req, res, next) {
    var text = req.body.text;      
    var url         = 'http://' + production_url + ':3000/'+'suggestive_phrases';
    var postData = {"text" : text};
    var options = {
        url: url,
        method: 'post',
        body: postData,
        json: true
    };

    request(options, function (err, resp, body) {
        if(err) {
            res.send(err);
        } else {
            res.send(body);
        }
    })
}

module.exports = {
    suggest: suggest,
};