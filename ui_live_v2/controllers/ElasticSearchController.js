var request = require("request");
var util  = require("util");

var staging_url = "52.220.121.25";
var production_url = "52.76.250.165"
function searchByTitle (req, res, next) {

    res.render('search/search', {
        title: 'Search',
        searchterm: req.params.searchterm
    })
}

function searchByTitleAjax(req, res, next){


    var posturl = req.body.url;
    var postbody = JSON.parse(req.body.body);
    var url         = 'http://' + production_url + ':3000/'+posturl;
    var postData    = {
        postbody: postbody,
        username: req.session.username
    };



    var options = {
        url: url,
        method: 'post',
        body: postData,
        json: true

    };

    
    request(options, function (err, resp, body) {
        if(err) {
            res.send(err);
        } else {
            res.send(body);
        }
    })
}
function searchByJudgement(req, res, next){


    var posturl = req.body.url;
   
    var postbody = JSON.parse(req.body.body);
    var url         = 'http://' + production_url + ':3000/'+posturl;
    var postData    = {
        postbody: postbody,
        username: req.session.username
    };


    var options = {
        url: url,
        method: 'post',
        body: postData,
        json: true

    };
    
    request(options, function (err, resp, body) {
        if(err) {
            res.send(err);
        } else {
            res.send(body);
        }
    })

}

function searchByTitleTax(req, res, next){
    
    var posturl = req.body.url;
    var postbody = JSON.parse(req.body.body);
    var url         = 'http://' + production_url + ':3000/' + posturl;
    var postData    = {
        postbody: postbody,
        username: req.session.username
    };


    var options = {
        url: url,
        method: 'post',
        body: postData,
        json: true

    };
    
    request(options, function (err, resp, body) {
        if(err) {
            res.send(err);
        } else {
            res.send(body);
        }
    })
}

function searchByTitleDelhi(req, res, next){
    
    var posturl = req.body.url;
    var postbody = JSON.parse(req.body.body);
    var url         = 'http://' + production_url + ':3000/' + posturl;
    var postData    = {
        postbody: postbody,
        username: req.session.username
    };

    var options = {
        url: url,
        method: 'post',
        body: postData,
        json: true

    };
    
    request(options, function (err, resp, body) {
        if(err) {
            res.send(err);
        } else {
            res.send(body);
        }
    })
}

function searchByJudgementDelhi(req, res, next){
    
    var posturl = req.body.url;
    var postbody = JSON.parse(req.body.body);
    var url         = 'http://' + production_url + ':3000/' + posturl;
     var postData    = {
        postbody: postbody,
        username: req.session.username
    };

    var options = {
        url: url,
        method: 'post',
        body: postData,
        json: true

    };
    
    request(options, function (err, resp, body) {
        if(err) {
            res.send(err);
        } else {
            res.send(body);
        }
    })
}

module.exports = {
    searchByTitle: searchByTitle,
    searchByTitleAjax : searchByTitleAjax,
    searchByJudgement : searchByJudgement,
    searchByTitleTax : searchByTitleTax,
    searchByTitleDelhi : searchByTitleDelhi,
    searchByJudgementDelhi : searchByJudgementDelhi,
};