var request = require('request');
var ip = '52.76.250.165' //API server
var port = 3000

function verify (req, res) {
    console.log(req.body);
    var postData = req.body;
    var url  = 'http://' + ip + ':' + port + '/verify';
    var options = {
        url: url,
        method: 'post',
        body: postData,
        json: true
    };
    console.log(options);
    request(options, function (err, resp, body) {
        if(err) {
            res.send("An error occured. Please try again later");
        } else {
            if(typeof(body)==="string")
            {
                res.send(body.toString());
            }
            else
            {
                req.session.username = body.email;
                req.session.user = body.name;
                res.send("true");
            }
        }
    })
}

module.exports = {
    verify: verify
};