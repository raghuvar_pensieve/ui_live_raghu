
function howitworks (req, res, next) {

    var loggedin = false
    if(req.session.username != null)
    {
        loggedin = true
    }
    res.render('product/howitworks', {
        loggedin:loggedin,
        username:req.session.user
    });
}

function faqs (req, res, next) {

    var loggedin = false
    if(req.session.username != null)
    {
        loggedin = true
    }
    res.render('product/faqs', {
        loggedin:loggedin,
        username:req.session.user
    });
}


module.exports = {
    howitworks: howitworks,
    faqs : faqs
};