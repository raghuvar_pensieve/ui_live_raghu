
function privacy (req, res, next) {

    var loggedin = false
    if(req.session.username != null)
    {
        loggedin = true
    }
    res.render('policy/privacy', {
        loggedin:loggedin,
        username:req.session.user
    });
}

function tnc (req, res, next) {

    var loggedin = false
    if(req.session.username != null)
    {
        loggedin = true
    }
    res.render('policy/tnc', {
        loggedin:loggedin,
        username:req.session.user
    });
}

function refund (req, res, next) {

    var loggedin = false
    if(req.session.username != null)
    {
        loggedin = true
    }
    res.render('policy/refund', {
        loggedin:loggedin,
        username:req.session.user
    });
}


module.exports = {
    privacy: privacy,
    tnc : tnc,
    refund : refund
};