var nodemailer = require('nodemailer');
var ses = require('nodemailer-ses-transport');
var request = require('request');
var production = '52.76.250.165';
var transporter = nodemailer.createTransport(ses({
	accessKeyId: 'AKIAJAC6H33BKDZD7XWQ',
	secretAccessKey: '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'
}));

function loadPage (req, res, next) {
	var info = 'welcome'

	res.render('requestDemo/requestDemo',{info: info});
}

function submitDemoData (req, res, next){

	var html1 = "Hi "+req.body.name +"," +
			"<br/>" +
			"<br/>"+
			"Thank you for showing interest in Mitra."+
			"<br>Our representative will contact you soon."+
			"<br>Have a great day!"+
			"<br>"+
			"<br/>Regards,"+
			"<br/><b>Mitra</b>"+
			"<br/>Artificial Intelligence Driven Legal Research Platform";

	var html2 = "Hi," +
			"<br/>" +
			"<br/>"+
			req.body.name+" has shown interest in Mitra."+
			"<br>The contact details are as follows:"+
			"<br>Name: "+req.body.name+
			"<br/>Email id: "+req.body.email+
			"<br/>Contact Number: "+req.body.contactNumber;

	var url = 'http://' + production + ':3000/dashboard/saveProspect';

    var postData = {
        'name': req.body.name,
        'email': req.body.email,
        'contact': req.body.contactNumber,
        'company': req.body.company
    }

    var options = {
        url: url,
        method: 'post',
        body: postData,
        json: true
    };      

    request(options,function(err,resp,body){

    	if(err){
			var err = "An error occured. Please try again later";
			res.render('requestDemo/requestDemo',{info: err})	
    	}
    	else{
    			transporter.sendMail({
					from: 'requestDemo@pensieve.co.in',
					to: req.body.email,
					subject: 'Welcome to Mitra',
					html: html1
				},function(err,status){
					if(err)
					{
						var err = "An error occured. Please try again later";
						res.render('requestDemo/requestDemo',{info: err})
					}
					else{
						var success = "Request sent!";
						transporter.sendMail({
							from: 'requestDemo@pensieve.co.in',
							to: 'requestDemo@pensieve.co.in',
							subject: 'Demo requested!',
							html: html2
						},function(err,status){
							if(err){
								var err = "An error occured. Please try again later";
								res.render('requestDemo/requestDemo',{info: err})
							}
							else{
								res.render('requestDemo/requestDemo',{info: success});		
							}
						});
					}
				});		
    	}
    });	
}

module.exports = {
	loadPage : loadPage,
	submitDemoData : submitDemoData
};