var request = require('request');
var production = '52.76.250.165';

function users (req, res, next) {

    var loggedin = false;
    if(req.session.username == null)
    {
        res.redirect('/login/0')
    }
    else{
        loggedin = true;
        var url = 'http://' + production + ':3000/dashboard';
        var postData = {
            username : req.session.username,
        }
        var options = {
            url: url,
            method: 'post',
            body: postData,
            json: true
        };      
        request(options, function (err, resp, body) {

            if(err) {
                console.log(err);
            } else if(body.allUsers) {
                console.log(body.allUsers[0]);
                res.render('dashboard/users', {
                    loggedin:loggedin,
                    username:req.session.user,
                    allUsers: body.allUsers
                });    
            }
            else{
                res.redirect('/');
            }
        });
    }
}

function prospects (req, res, next) {

    var loggedin = false
    if(req.session.username == null)
    {
        res.redirect('/login/0')
    }
    else{
        loggedin = true;
        var url = 'http://' + production + ':3000/dashboard/prospects';
        var postData = {
            username : req.session.username,
        }
        var options = {
            url: url,
            method: 'post',
            body: postData,
            json: true
        };      
        request(options, function (err, resp, body) {
            console.log(body);
            if(err) {
                console.log(err);
            } else if(body.allProspects) {
                res.render('dashboard/prospects', {
                    loggedin:loggedin,
                    username:req.session.user,
                    allProspects: body.allProspects
                });    
            }
            else{
                res.redirect('/');
            }
        });
    }
}


module.exports = {
    users: users,
    prospects : prospects
};