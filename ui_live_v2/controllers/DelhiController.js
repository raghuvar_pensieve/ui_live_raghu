var request = require('request');
const util = require('util')

var production = '52.76.250.165'
var staging = '52.220.121.25'

function home (req, res, next) {

    // return res.send("WELCOME TO REST API UPDATE");
    var loggedin = false
    if(req.session.username != null)
    {
        loggedin = true
    }
    res.render('home/delhi_home', {
        loggedin:loggedin,
        username:req.session.user,
        title: 'Mitra'
    })

}

function search (req, res, next) {

    // return res.send("WELCOME TO REST API UPDATE");
    var loggedin = false
    if(req.session.username != null)
    {
        loggedin = true
    }
    console.log(req.params);
    res.render('search/delhi_search', {
        loggedin:loggedin,
        username:req.session.user,
        title: 'Delhi SC Search',
        searchterm: req.params.searchterm
    })
}


function delhi_search (req, res, next) {
    console.log(req.body);
    console.log(" in the delhi part");
    var posturl = req.body.url;
    console.log("Post URL :: " + posturl)
    var postbody = JSON.parse(req.body.body);
    var url         = 'http://' + staging + ':3000/'+ posturl;
//    var url         = 'http://127.0.0.1' + ':3000/'+ posturl;
    var postData    = postbody;

    console.log("url :: " + url);

    console.log(postbody,postData);

    var options = {
        url: url,
        method: 'post',
        body: postData,
        json: true
    };
    console.log(options);
    request(options, function (err, resp, body) {
        if(err) {

            res.send(err);
        } else {
            console.log('body recived');
            console.log(body);
            res.send(body);

        }
    })
}

module.exports = {
    search: search,
    home : home,
    delhi_search : delhi_search
};