
function about (req, res, next) {

    var loggedin = false
    if(req.session.username != null)
    {
        loggedin = true
    }
    res.render('company/aboutUs', {
        loggedin:loggedin,
        username:req.session.user
    });
}

function contact (req, res, next) {

    var loggedin = false
    if(req.session.username != null)
    {
        loggedin = true
    }
    res.render('company/contactUs', {
        loggedin:loggedin,
        username:req.session.user
    });
}


module.exports = {
    about: about,
    contact : contact
};