var path = require('path');
module.exports = {
	entry: './views/myAccount/App.js',
	output: {
		path: path.resolve(__dirname,'public/js'),
		filename:'App_bundle.js'
	},
	module:{
		loaders: [
			{
				test: /\.(jpe?g|png|gif|svg)$/i,
				loaders: [
					'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
					'image-webpack-loader?bypassOnDebug&optimizationLevel=7&interlaced=false'
				]
			}
		],
		rules:[
			{test: /\.(js)$/,use:'babel-loader'},
			{test: /\.css$/,use:['style-loader','css-loader']}
		]
	},	
	watch: true
}