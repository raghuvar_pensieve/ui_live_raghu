module.exports = function(app) {

    var policyController = require('../controllers/PolicyController');

    app.get('/policy/privacy', policyController.privacy);
    app.get('/policy/tnc', policyController.tnc);
    app.get('/policy/refund', policyController.refund);
};