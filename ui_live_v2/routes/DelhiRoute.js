module.exports = function(app) {

    var DelhiController = require('../controllers/DelhiController');

    app.get('/delhi/search/:searchterm', DelhiController.search);
    app.get('/delhi', DelhiController.home) ;
    app.post('/delhi_search', DelhiController.delhi_search);
};