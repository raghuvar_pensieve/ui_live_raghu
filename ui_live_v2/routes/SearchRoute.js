module.exports = function(app) {

    var SearchController = require('../controllers/SearchController');

    app.get('/search/:searchterm', SearchController.search);

};