module.exports = function(app) {

    var RelationController = require('../controllers/RelationController');

    app.get('/relation/:data_type/:id', RelationController.detail);
    app.get('/render/relation/:data_type/:id',RelationController.renderCase);


};