module.exports = function(app) {

    var SubscribeController = require('../controllers/SubscribeController');

    app.post('/verify', SubscribeController.verify);
};