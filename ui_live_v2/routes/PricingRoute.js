module.exports = function(app) {

    var PricingController = require('../controllers/PricingController');

    app.get('/pricing', PricingController.pricing);
};