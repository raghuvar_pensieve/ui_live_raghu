module.exports = function(app) {

    var USController = require('../controllers/USController');

    app.get('/us/search/:searchterm', USController.search);
    app.get('/us', USController.home) ;
    app.post('/us_search', USController.us_search);
};