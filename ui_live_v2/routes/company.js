module.exports = function(app) {

    var companyController = require('../controllers/companyController');

    app.get('/company/about', companyController.about);
    app.get('/company/contact', companyController.contact);
};