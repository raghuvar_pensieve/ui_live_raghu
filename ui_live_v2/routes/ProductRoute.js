module.exports = function(app) {

    var ProductController = require('../controllers/ProductController');

    app.get('/product/howitworks', ProductController.howitworks);
    app.get('/product/faqs', ProductController.faqs);
};