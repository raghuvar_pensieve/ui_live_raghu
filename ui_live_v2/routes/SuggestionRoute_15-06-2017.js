module.exports = function(app) {

    var SuggestionController = require('../controllers/SuggestionController');

    app.post('/suggest_phrase', SuggestionController.suggest);

};