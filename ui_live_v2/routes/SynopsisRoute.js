module.exports = function(app) {

    var SynopsisController = require('../controllers/SynopsisController');

    app.get('/synopsis', SynopsisController.detail);

    app.post('/summarise', SynopsisController.summarise);

};