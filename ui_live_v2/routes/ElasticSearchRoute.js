module.exports = function(app) {

    var ElasticSearchController = require('../controllers/ElasticSearchController');

    app.get('/searchByTitle/:searchItem', ElasticSearchController.searchByTitle);

    app.post('/ByTitle', ElasticSearchController.searchByTitleAjax);
    app.post('/ByJudgement', ElasticSearchController.searchByJudgement);

    app.post('/ByTitleTax', ElasticSearchController.searchByTitleTax);

    app.post('/ByTitleDelhi', ElasticSearchController.searchByTitleDelhi);
    app.post('/ByJudgementDelhi', ElasticSearchController.searchByJudgementDelhi);


    app.post('/ByTitleMumbai', ElasticSearchController.searchByTitleAjax);
    app.post('/ByJudgementMumbai', ElasticSearchController.searchByJudgement);

    app.post('/ByTitleUS', ElasticSearchController.searchByTitleAjax);
    app.post('/ByJudgementUS', ElasticSearchController.searchByJudgement);

};