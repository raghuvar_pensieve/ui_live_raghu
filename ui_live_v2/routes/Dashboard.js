module.exports = function(app) {

    var DashboardController = require('../controllers/DashboardController');

    app.get('/dashboard/users', DashboardController.users);
    app.get('/dashboard/prospects', DashboardController.prospects);
};