module.exports = function(app) {

    var MumbaiController = require('../controllers/MumbaiController');

    app.get('/mumbai/search/:searchterm', MumbaiController.search);
    app.get('/mumbai', MumbaiController.home) ;
    app.post('/mumbai_search', MumbaiController.mumbai_search);
};