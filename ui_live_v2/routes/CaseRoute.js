module.exports = function(app) {

    var CaseController = require('../controllers/CaseController');

    app.get('/case', CaseController.detail);
    app.post('/case/similarByText',CaseController.similarByText);
    

};
