module.exports = function(app) {

    var HomeController = require('../controllers/HomeController');

    app.get('/', HomeController.home);
    app.post('/ajax', HomeController.ajax);
    

};