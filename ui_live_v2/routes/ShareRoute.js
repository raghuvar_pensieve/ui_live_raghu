module.exports = function(app) {

    var ShareController = require('../controllers/ShareController');
    app.post('/ShareGmail', ShareController.ShareGmail);
    app.post('/ShareLinkedIn', ShareController.ShareLinkedIn);
    app.post('/ShareWhatsApp', ShareController.ShareWhatsApp);
};